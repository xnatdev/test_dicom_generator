import org.nrg.dicom.generator.BatchSpecification
import org.nrg.dicom.generator.CStoreWrapper

import java.nio.file.Path
import java.nio.file.Paths

@GrabResolver(name='dcm4che repo', root='https://www.dcm4che.org/maven2', m2Compatible=true)
@Grapes([
        @Grab(group='com.fasterxml.jackson.core', module='jackson-databind', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.dataformat', module='jackson-dataformat-yaml', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.datatype', module='jackson-datatype-jsr310', version='2.10.2'),
        @Grab(group='commons-cli', module='commons-cli', version='1.4'),
        @Grab(group='org.dcm4che.tool', module='dcm4che-tool-storescu', version='5.21.0'),
        @Grab(group='org.apache.commons', module='commons-lang3', version='3.10'),
        @Grab(group='org.apache.commons', module='commons-math3', version='3.6.1')
])
final CliBuilder cliBuilder = new CliBuilder(usage: 'CStoreMonitor.groovy <parameters>')

cliBuilder.with {
    h('Host to receive the DICOM instances', args: 1, required: true)
    p('Port for the DICOM AE', args: 1, required: true)
    a('Called AE title to specify in transmission to the DICOM AE', args: 1, required: true)
    c('Calling AE title to use in transmission to the DICOM AE', args: 1, required: false)
    f('Comma-separated list of batches to send to the DICOM AE', args: 1, required: true)
}

final OptionAccessor options = cliBuilder.parse(args)
final Path recordingPath = Paths.get('./time_recording')
final File instanceCumulativeFile = recordingPath.resolve('instance_cumulative.txt').toFile()
final File patientCumulativeFile = recordingPath.resolve('patient_cumulative.txt').toFile()
final File patientMarginalFile = recordingPath.resolve('patient_marginal.txt').toFile()
recordingPath.toFile().mkdir()
[instanceCumulativeFile, patientCumulativeFile, patientMarginalFile].each { file ->
    if (file.exists()) {
        file.delete()
    }
}

long patientsSent = 0
long instancesSent = 0
double cumulativeTime = 0
(options.f as String).split(',').each { batch ->
    new File(batch).eachDir { layerDir ->
        final double layerTime = (new CStoreWrapper().send(options.h as String, options.p as int, options.a as String, layerDir, options.c ?: 'NRG_GEN') / 1000.0 as double).round(1)
        println("Instances in ${batch}/${layerDir.name} have been sent to PACS. Time elapsed for this layer: ${layerTime} s.")
        patientsSent += BatchSpecification.MAX_PATIENTS_PER_LAYER // will not be accurate for at MOST one layer. That's fine. It should be the last layer, so we won't even get that far
        instancesSent += layerDir.listFiles().length
        cumulativeTime = (cumulativeTime + layerTime).round(1)
        instanceCumulativeFile << "(${instancesSent}, ${cumulativeTime})\n"
        patientCumulativeFile << "(${patientsSent}, ${cumulativeTime})\n"
        patientMarginalFile << "(${patientsSent}, ${layerTime})\n"
    }
}
