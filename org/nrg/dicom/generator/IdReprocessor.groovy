package org.nrg.dicom.generator

import org.nrg.dicom.generator.metadata.SortableStudy

class IdReprocessor {

    void reprocessIds(long basePatientId, long baseStudyId, List<SortableStudy> studies) {
        long currentPatientId = basePatientId
        long currentStudyId = baseStudyId
        final Map<String, String> patientUidIdMap = [:]
        final Map<String, String> studyUidIdMap = [:]
        final YamlObjectMapper yamlMapper = new YamlObjectMapper()

        studies.sort(true).each { study ->
            studyUidIdMap.put(study.studyInstanceUID, currentStudyId.toString())
            currentStudyId++
            if (!patientUidIdMap.containsKey(study.patientInstanceUID)) {
                patientUidIdMap.put(study.patientInstanceUID, currentPatientId.toString())
                currentPatientId++
            }
        }

        new File('batches').eachFile { batch ->
            final BatchSpecification batchSpecification = yamlMapper.readValue(batch, BatchSpecification)
            batchSpecification.patients.each { patient ->
                patient.setPatientId(patientUidIdMap.get(patient.patientInstanceUid))
                patient.studies.each { study ->
                    final String id = studyUidIdMap.get(study.studyInstanceUid)
                    study.setStudyId(id)
                    study.setAccessionNumber(id)
                }
            }
            batchSpecification.writeToFile()
        }
    }

}
