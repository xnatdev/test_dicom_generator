package org.nrg.dicom.generator.util

class StringReplacements {

    public static final BODYPART = '%BODYPART%'
    public static final LATERALITY = '%LATERALITY%'
    public static final VIEW = '%VIEW%'
    public static final FPS = '%FPS%'

}
