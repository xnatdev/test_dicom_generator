package org.nrg.dicom.generator

import com.fasterxml.jackson.annotation.JsonIgnore
import org.nrg.dicom.generator.metadata.Patient

import java.nio.file.Path

class BatchSpecification {

    int id
    List<Patient> patients = []
    @JsonIgnore written = false
    public static final int MAX_PATIENTS = 10000
    public static final int MAX_PATIENTS_PER_LAYER = 100

    @JsonIgnore
    boolean isFull() {
        patients.size() > MAX_PATIENTS // TODO: what if a few patients have many, many, many studies?
    }

    String relativeFilePath() {
        "batches/batch_${id}.yaml"
    }

    void writeToFile() {
        new YamlObjectMapper().writeValue(new File(relativeFilePath()), this)
        written = true
    }

    void writeToFileAndLog(int expectedNumBatches) {
        writeToFile()
        println("  Specification for batch #${id} (out of roughly ${expectedNumBatches}) has been created in ${relativeFilePath()}")
    }

    void generateDicom(int index, int totalNumBatches, File outputDir) {
        println("  Writing batch with id ${id} to DICOM...")
        final Path batchDirectory = outputDir.toPath().resolve("batch_${id}")
        batchDirectory.toFile().mkdir()
        int layerId = -1
        int fileId
        int patientsWritten = 0
        Path layer

        patients.each { patient ->
            if (patientsWritten % MAX_PATIENTS_PER_LAYER == 0) {
                layerId++
                fileId = 1
                layer = batchDirectory.resolve("layer_${layerId}")
                layer.toFile().mkdir()
            }

            patient.studies.each { study ->
                if (patient.ethnicGroup != null) {
                    study.setEthnicGroup(patient.ethnicGroup.sampleEncodedValue())
                }
                study.resolvePrivateElements(patient, [])
                study.series.each { series ->
                    series.instances.each { instance ->
                        instance.writeToDicomFile(patient, study, series, layer.resolve("${fileId}.dcm").toFile())
                        fileId++
                    }
                }
            }
            patientsWritten++
        }
        println("  Successfully wrote batch with id ${id} to DICOM [${index + 1}/${totalNumBatches}]")
    }

}
