package org.nrg.dicom.generator

import org.apache.commons.lang3.time.StopWatch
import org.dcm4che3.data.Attributes
import org.dcm4che3.net.ApplicationEntity
import org.dcm4che3.net.Connection
import org.dcm4che3.net.Device
import org.dcm4che3.tool.storescu.StoreSCU

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class CStoreWrapper {

    int send(String host, int port, String aeTitle, File directory, String callingAeTitle) {
        final Device localDevice = new Device(callingAeTitle)
        final Connection connection = new Connection()
        final ApplicationEntity localAE = new ApplicationEntity(callingAeTitle)
        localDevice.addApplicationEntity(localAE)
        localDevice.addConnection(connection)
        localAE.addConnection(connection)
        final StoreSCU storeSCU = new StoreSCU(localAE)
        final ExecutorService executorService = Executors.newSingleThreadExecutor()
        final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
        localDevice.setExecutor(executorService)
        localDevice.setScheduledExecutor(scheduledExecutorService)
        storeSCU.getAAssociateRQ().setCalledAET(aeTitle)
        final Connection remoteConnection = storeSCU.getRemoteConnection()
        remoteConnection.setHostname(host)
        remoteConnection.setPort(port)
        storeSCU.scanFiles([directory.absolutePath], false)
        storeSCU.setAttributes(new Attributes())
        final StopWatch stopWatch = new StopWatch()
        stopWatch.start()
        storeSCU.open()
        storeSCU.sendFiles()
        storeSCU.close()
        stopWatch.stop()
        executorService.shutdown()
        scheduledExecutorService.shutdown()
        println()
        stopWatch.getTime(TimeUnit.MILLISECONDS)
    }

}
