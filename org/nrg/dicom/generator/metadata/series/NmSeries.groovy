package org.nrg.dicom.generator.metadata.series

import org.dcm4che3.data.Attributes
import org.nrg.dicom.generator.metadata.Series

class NmSeries extends Series implements SupportsNmPetPatientOrientation {

    @Override
    void encode(Attributes attributes) {
        super.encode(attributes)
        encodePatientOrientation(attributes)
    }

}
