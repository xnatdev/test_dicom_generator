package org.nrg.dicom.generator.metadata.series

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.nrg.dicom.generator.metadata.Series

class SrSeries extends Series implements SupportsMPPS {

    @Override
    void encode(Attributes attributes) {
        super.encode(attributes)
        if (referencedPerformedProcedureStepSopInstanceUid != null) {
            encodeMPPS(attributes)
        } else {
            attributes.newSequence(Tag.ReferencedPerformedProcedureStepSequence, 0) // specified as type 2 in SR Series Module
        }
    }

}
