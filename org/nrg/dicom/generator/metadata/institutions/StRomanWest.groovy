package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class StRomanWest extends Institution {

    @Override
    String getInstitutionName() {
        'St Roman West'
    }

    @Override
    String getInstitutionAddress() {
        '4599 W Hammerton, St. Louis, MO 63130'
    }

}
