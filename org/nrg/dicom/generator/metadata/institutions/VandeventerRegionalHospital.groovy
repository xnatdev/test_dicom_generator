package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class VandeventerRegionalHospital extends Institution {

    @Override
    String getInstitutionName() {
        'Vandeventer Regional Hospital'
    }

    @Override
    String getInstitutionAddress() {
        '4 Green Plaza, St. Louis, MO 63106'
    }

}
