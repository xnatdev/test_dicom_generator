package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class KoNullInstitution extends Institution {

    @Override
    String getInstitutionName() {
        null
    }

    @Override
    String getInstitutionAddress() {
        null
    }

}
