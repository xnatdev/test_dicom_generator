package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.enums.Nationality

class SeoulInterventionalRadiologyClinic extends Institution {

    @Override
    String getInstitutionName() {
        'Seoul Interventional Radiology Clinic'
    }

    @Override
    String getInstitutionAddress() {
        '1484-9, Gayang 3(sam)-dong, Gangseo-gu, Seoul, Korea'
    }

    @Override
    Nationality commonNationality() {
        Nationality.KOREAN
    }

}
