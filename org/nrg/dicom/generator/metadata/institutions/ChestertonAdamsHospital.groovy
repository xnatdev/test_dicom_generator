package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class ChestertonAdamsHospital extends Institution {

    @Override
    String getInstitutionName() {
        'Chesterton-Adams Hospital'
    }

    @Override
    String getInstitutionAddress() {
        '114 N Becker Rd, St. Louis, MO 63105'
    }

}
