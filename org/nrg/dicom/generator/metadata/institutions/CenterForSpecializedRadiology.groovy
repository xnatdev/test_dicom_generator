package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class CenterForSpecializedRadiology extends Institution {

    @Override
    String getInstitutionName() {
        'Center for Specialized Radiology'
    }

    @Override
    String getInstitutionAddress() {
        '1 Compton Rd, San Francisco, CA 94102'
    }

}
