package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class RtNullInstitution extends Institution {

    @Override
    String getInstitutionName() {
        null
    }

    @Override
    String getInstitutionAddress() {
        null
    }

}
