package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class WestAthensGeneralHospital extends Institution {

    @Override
    String getInstitutionName() {
        'West Athens General Hospital'
    }

    @Override
    String getInstitutionAddress() {
        null // TODO ?
    }

}
