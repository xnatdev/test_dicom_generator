package org.nrg.dicom.generator.metadata.institutions

import org.nrg.dicom.generator.metadata.Institution

class SouthGrandMammographySpecialty extends Institution {

    @Override
    String getInstitutionName() {
        'SGMS'
    }

    @Override
    String getInstitutionAddress() {
        null
    }

}
