package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.metadata.scanners.GreekSiemensAvanto
import org.nrg.dicom.generator.metadata.seriesTypes.mr.Localizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T1Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T2Weighted

class GreekMri extends MriMinimalProtocol {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        final Localizer localizer = new Localizer() {
            @Override
            List<Class<? extends Equipment>> getCompatibleEquipment() {
                [GreekSiemensAvanto]
            }
        }
        final T1Weighted t1 = new T1Weighted() {
            @Override
            List<Class<? extends Equipment>> getCompatibleEquipment() {
                [GreekSiemensAvanto]
            }
        }
        final T2Weighted t2 = new T2Weighted() {
            @Override
            List<Class<? extends Equipment>> getCompatibleEquipment() {
                [GreekSiemensAvanto]
            }
        }
        localizer.setAnatomicalPlane(AnatomicalPlane.TRANSVERSE)
        t1.setAnatomicalPlane(AnatomicalPlane.TRANSVERSE)
        t2.setAnatomicalPlane(AnatomicalPlane.TRANSVERSE)
        [localizer, t1, t2]
    }

    @Override
    boolean isXnatCompatible() {
        false
    }

    @Override
    boolean isApplicableFor(Patient patient) {
        patient.nationality == Nationality.GREEK
    }

}
