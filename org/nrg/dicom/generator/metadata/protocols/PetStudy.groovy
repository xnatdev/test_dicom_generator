package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.enums.contextGroups.PetRadiopharmaceutical
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

abstract class PetStudy extends Protocol {

    PetRadiopharmaceutical currentPharmaceutical
    int currentInjectionOffset
    double currentDosage
    private static final EnumeratedDistribution<PetRadiopharmaceutical> radiopharmaceuticalRandomizer = RandomUtils.setupWeightedLottery([
            (PetRadiopharmaceutical.FLUORODEOXYGLUCOSE_F_18_) : 90,
            (PetRadiopharmaceutical.PITTSBURGH_COMPOUND_B_C_11_) : 10
    ])

    @Override
    void resample(Patient patient) {
        currentPharmaceutical = radiopharmaceuticalRandomizer.sample()
        currentInjectionOffset = currentPharmaceutical.scanDelayTime + ThreadLocalRandom.current().nextInt(-120, 121)
        currentDosage = ThreadLocalRandom.current().nextDouble(470000000, 530000000)
    }

}
