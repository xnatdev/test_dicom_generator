package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.cr.AnteroposteriorCrSeries
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class AnteroposteriorCr extends Protocol {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("XR ${BODYPART} 1V PORTABLE".toString()) : 70,
            ("XR ${BODYPART} 1 VIEW AP OR PA".toString()) : 30
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new AnteroposteriorCrSeries()]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.ABDOMEN, BodyPart.CHEST]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(randomizer, bodyPart)
    }

}
