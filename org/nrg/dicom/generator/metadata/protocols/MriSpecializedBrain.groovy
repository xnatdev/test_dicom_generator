package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.mr.*
import org.nrg.dicom.generator.util.RandomUtils

class MriSpecializedBrain extends Protocol {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'MRI BRAIN WITH AND WITHOUT CONTRAST' : 50,
            'MR BRAIN WO+W CONTRAST' : 30,
            'MRI Diff/Perf Weighted' : 30,
            'mri_w_wo_contrast' : 20
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Localizer(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new T1Weighted(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new SusceptibilityWeightedImage(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new DynamicContrastEnhanced(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new ApparentDiffusionCoefficient(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new DynamicSusceptibilityContrast(anatomicalPlane: AnatomicalPlane.SAGITTAL)
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.BRAIN]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

}
