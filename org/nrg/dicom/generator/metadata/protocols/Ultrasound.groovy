package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.us.UsSeriesType

class Ultrasound extends Protocol {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new UsSeriesType()]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        null // not usually encoded in the DICOM data for ultrasounds
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        null // not usually encoded in the DICOM data for ultrasounds
    }

}
