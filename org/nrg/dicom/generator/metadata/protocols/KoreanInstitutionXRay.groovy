package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.metadata.scanners.KoreanInstitutionGEDiscoveryXR656
import org.nrg.dicom.generator.metadata.seriesTypes.dx.DigitalXRayForPresentation

class KoreanInstitutionXRay extends SingleViewXRay {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new DigitalXRayForPresentation() {
            @Override
            List<Class<? extends Equipment>> getCompatibleEquipment() {
                [KoreanInstitutionGEDiscoveryXR656]
            }
        }]
    }

    @Override
    boolean isApplicableFor(Patient patient) {
        patient.nationality == Nationality.KOREAN
    }

    @Override
    boolean isXnatCompatible() {
        false
    }

}
