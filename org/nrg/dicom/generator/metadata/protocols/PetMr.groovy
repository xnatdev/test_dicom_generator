package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.mr.DiffusionTensorImage
import org.nrg.dicom.generator.metadata.seriesTypes.mr.FluidAttenuatedInversionRecovery
import org.nrg.dicom.generator.metadata.seriesTypes.mr.Localizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.SusceptibilityWeightedImage
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T1Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.mr.ThreePlaneLocalizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.UltrashortEchoTimeMrac
import org.nrg.dicom.generator.metadata.seriesTypes.pt.MracPetNonAttenuationCorrection
import org.nrg.dicom.generator.metadata.seriesTypes.pt.MracPetWithAttenuationCorrection
import org.nrg.dicom.generator.metadata.seriesTypes.sr.PhoenixZIPReport
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class PetMr extends PetStudy {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            ("MR_PET_${BODYPART}".toString()) : 50,
            ("Clinical^${BODYPART}".toString()) : 30,
            ("MR-PET ${BODYPART}".toString()) : 15,
            'PET-MR Study' : 5
    ])

    private static final List<SeriesType> seriesTypes = [
            new Localizer(),
            new FluidAttenuatedInversionRecovery(),
            new T1Weighted(anatomicalPlane : AnatomicalPlane.TRANSVERSE),
            new DiffusionTensorImage(anatomicalPlane : AnatomicalPlane.TRANSVERSE),
            new UltrashortEchoTimeMrac(),
            new MracPetNonAttenuationCorrection(),
            new MracPetWithAttenuationCorrection(),
            new SusceptibilityWeightedImage(),
            new PhoenixZIPReport()
    ]

    @Override
    List<SeriesType> getAllSeriesTypes() {
        seriesTypes
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [
                BodyPart.ABDOMEN,
                BodyPart.CHEST,
                BodyPart.HEAD,
                BodyPart.PELVIS
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(studyDescriptionRandomizer, bodyPart)
    }

    @Override
    void resample(Patient patient) {
        super.resample(patient)
        seriesTypes.remove(0)
        seriesTypes.add(0, ThreadLocalRandom.current().nextBoolean() ? new Localizer(anatomicalPlane: AnatomicalPlane.TRANSVERSE) : new ThreePlaneLocalizer())
    }

}
