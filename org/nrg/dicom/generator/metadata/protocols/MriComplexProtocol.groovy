package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.seriesTypes.mr.BloodOxygenLevelDependentImage
import org.nrg.dicom.generator.metadata.seriesTypes.mr.DiffusionTensorImage
import org.nrg.dicom.generator.metadata.seriesTypes.mr.DiffusionWeightedImage
import org.nrg.dicom.generator.metadata.seriesTypes.mr.FluidAttenuatedInversionRecovery
import org.nrg.dicom.generator.metadata.seriesTypes.mr.Localizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T1Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T2Star
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T2Weighted
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class MriComplexProtocol extends Protocol {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            ("MRI ${BODYPART} WITH AND WITHOUT CONTRAST".toString()) : 50,
            ("MR ${BODYPART} WO+W CONTRAST".toString()) : 30,
            ("MRI ${BODYPART}".toString()) : 10,
            ("${BODYPART} MR IMAG".toString()) : 5
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Localizer(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new T1Weighted(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new T2Weighted(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new DiffusionWeightedImage(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new DiffusionTensorImage(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new FluidAttenuatedInversionRecovery(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new T2Star(anatomicalPlane: AnatomicalPlane.SAGITTAL),
                new T1Weighted(anatomicalPlane: AnatomicalPlane.TRANSVERSE).withContrast(),
                new BloodOxygenLevelDependentImage(anatomicalPlane: AnatomicalPlane.CORONAL)
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [
                BodyPart.ABDOMEN,
                BodyPart.BRAIN,
                BodyPart.CHEST,
                BodyPart.HEAD,
                BodyPart.HEART
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(studyDescriptionRandomizer, bodyPart)
    }

}
