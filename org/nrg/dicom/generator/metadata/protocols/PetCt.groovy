package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.ct.CtWithAttenuationCorrection
import org.nrg.dicom.generator.metadata.seriesTypes.ct.PatientProtocol
import org.nrg.dicom.generator.metadata.seriesTypes.ct.Topogram
import org.nrg.dicom.generator.metadata.seriesTypes.ot.SiemensProprietaryCtFusion
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtNonAttenuationCorrected
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtWithAttenuationCorrection
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class PetCt extends PetStudy {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("PET^PETCT_AC_NAC_${BODYPART}".toString()) : 30,
            ("PETCT^${BODYPART}".toString()) : 20,
            ("PET^01_PETCT_${BODYPART} (Adult)".toString()) : 20,
            'PET/CT^ADULT' : 15,
            'PET-CT STUDY' : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Topogram(),
                new SiemensProprietaryCtFusion(),
                new CtWithAttenuationCorrection(),
                new PtWithAttenuationCorrection(),
                new PtNonAttenuationCorrected(),
                new PatientProtocol()
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [
                BodyPart.ABDOMEN,
                BodyPart.BRAIN,
                BodyPart.HEAD,
                BodyPart.THORAX,
                BodyPart.WHOLEBODY
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(randomizer, bodyPart)
    }

}
