package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.enums.BodyPart

abstract class SecondaryCaptureStudy extends Protocol {

    @Override
    List<BodyPart> getApplicableBodyParts() {
        null
    }

    @Override
    boolean includeMedicalStaff() {
        false
    }

    @Override
    boolean allowPatientPhysiqueEncoding() {
        false
    }

}
