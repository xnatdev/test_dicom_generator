package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.scanners.XaScanner
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.xa.GenericAngiography

class XRayAngiography extends Protocol {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        (1 .. 10).collect {
            new GenericAngiography()
        }
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        null
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        (scanner as XaScanner).studyDescription
    }

}
