package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.multiple.SecondaryCaptureXRaySeries
import org.nrg.dicom.generator.util.RandomUtils

class SecondaryCaptureXRayStudy extends SecondaryCaptureStudy {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'XR 1 VIEW' : 30,
            'OUTSIDE EXAM' : 20,
            'SCANNED XRAY' : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new SecondaryCaptureXRaySeries()]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

}
