package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.nm.MyometrixResults
import org.nrg.dicom.generator.metadata.seriesTypes.nm.NmGatedTomo
import org.nrg.dicom.generator.metadata.seriesTypes.nm.NmTomo
import org.nrg.dicom.generator.metadata.seriesTypes.nm.RestingNm
import org.nrg.dicom.generator.util.RandomUtils

class MyocardialPerfusion extends Protocol {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'NM MYOCARDIAL PERFUSION SPECT MULTIPLE' : 100,
            'NM MPI SPECT' : 50,
            'MPI' : 30
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new RestingNm(),
                new NmTomo(),
                new NmGatedTomo(),
                new MyometrixResults()
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.HEART]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizer.sample()
    }

}
