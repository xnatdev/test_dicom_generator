package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.seriesTypes.xa.GenericAngiography
import org.nrg.dicom.generator.metadata.seriesTypes.xa.OriginalFluoroscopy

class OutsideXRayAngiography extends XRayAngiography {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        (1 .. 15).collect { index ->
            index in [0, 1, 6] ? new OriginalFluoroscopy() : new GenericAngiography()
        }
    }

}
