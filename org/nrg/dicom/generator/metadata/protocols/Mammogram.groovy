package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.Sex

abstract class Mammogram extends Protocol {

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.BREAST]
    }

    @Override
    boolean isApplicableFor(Patient patient) {
        patient.sex == Sex.FEMALE
    }

}
