package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.seriesTypes.mr.Localizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T1Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T2Weighted
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class MriMinimalProtocol extends Protocol {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            ("MRI ${BODYPART} WITHOUT CONTRAST".toString()) : 50,
            ("MRI ${BODYPART} EXPEDITED PROTOCOL".toString()) : 25,
            ("MR ${BODYPART} WO CONTRAST".toString()) : 30,
            ("MRI ${BODYPART} W/O CONTRAST".toString()) : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Localizer(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new T2Weighted(anatomicalPlane: AnatomicalPlane.TRANSVERSE),
                new T1Weighted(anatomicalPlane: AnatomicalPlane.TRANSVERSE)
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [
                BodyPart.BRAIN,
                BodyPart.CHEST,
                BodyPart.HEAD,
                BodyPart.HEART
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(studyDescriptionRandomizer, bodyPart)
    }

}
