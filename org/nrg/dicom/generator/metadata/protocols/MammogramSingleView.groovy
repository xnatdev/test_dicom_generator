package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.Laterality
import org.nrg.dicom.generator.metadata.enums.contextGroups.MammographyView
import org.nrg.dicom.generator.metadata.seriesTypes.mg.DigitalMammographyXRayForPresentation
import org.nrg.dicom.generator.util.RandomUtils

class MammogramSingleView extends Mammogram {

    private static final List<SeriesType> possibleSeriesTypes = cacheSeries()
    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'Diag Mammogram Unilateral' : 15,
            'LIMITED MAMMOGRAM, UNILATERAL' : 5,
            'Screen unilateral' : 3,
            'DIGITAL MAMMOGRAM,UNILATERAL' : 3
    ])

    @Override
    void resample(Patient patient) {
        setSeriesTypes(getAllSeriesTypes())
    }

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [RandomUtils.randomListEntry(possibleSeriesTypes)]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

    private static List<SeriesType> cacheSeries() {
        [MammographyView.ML, MammographyView.MLO, MammographyView.LM, MammographyView.LMO, MammographyView.CC, MammographyView.XCCM].collectMany { position ->
            [Laterality.LEFT, Laterality.RIGHT].collect { laterality ->
                new DigitalMammographyXRayForPresentation(laterality, position)
            }
        }
    }

}
