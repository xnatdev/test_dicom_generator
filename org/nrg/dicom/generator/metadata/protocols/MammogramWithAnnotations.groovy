package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.seriesTypes.pr.GspsMammographyAnnotations

class MammogramWithAnnotations extends MammogramSingleView {

    private static final SeriesType annotations = new GspsMammographyAnnotations()

    @Override
    List<SeriesType> getAllSeriesTypes() {
        super.getAllSeriesTypes() + [annotations] as List<SeriesType>
    }

}
