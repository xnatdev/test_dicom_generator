package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.Sex
import org.nrg.dicom.generator.metadata.seriesTypes.multiple.SecondaryCaptureMammogramSeries
import org.nrg.dicom.generator.util.RandomUtils

class SecondaryCaptureMammogramStudy extends SecondaryCaptureStudy {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'MAMMOGRAM 1 VIEW' : 30,
            'OUTSIDE EXAM' : 20,
            'Screen unilateral' : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new SecondaryCaptureMammogramSeries()]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.BREAST]
    }

    @Override
    boolean isApplicableFor(Patient patient) {
        patient.sex == Sex.FEMALE
    }

}
