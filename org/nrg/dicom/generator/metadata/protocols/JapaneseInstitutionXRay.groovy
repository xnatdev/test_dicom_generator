package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.metadata.scanners.JapaneseInstitutionGEDiscoveryXR656
import org.nrg.dicom.generator.metadata.seriesTypes.dx.DigitalXRayForPresentation

class JapaneseInstitutionXRay extends SingleViewXRay {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new DigitalXRayForPresentation() {
            @Override
            List<Class<? extends Equipment>> getCompatibleEquipment() {
                [JapaneseInstitutionGEDiscoveryXR656]
            }
        }]
    }

    @Override
    boolean isApplicableFor(Patient patient) {
        patient.nationality == Nationality.JAPANESE
    }

    @Override
    boolean isXnatCompatible() {
        false
    }

}
