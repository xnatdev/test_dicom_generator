package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.scanners.OutsideGEInfinia
import org.nrg.dicom.generator.metadata.seriesTypes.nm.MyometrixResults
import org.nrg.dicom.generator.metadata.seriesTypes.nm.NmGatedTomo
import org.nrg.dicom.generator.metadata.seriesTypes.nm.NmTomo
import org.nrg.dicom.generator.metadata.seriesTypes.nm.RestingNm

class OutsideMyocardialPerfusion extends MyocardialPerfusion {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new RestingNm() {
                    @Override
                    List<Class<? extends Equipment>> getCompatibleEquipment() {
                        [OutsideGEInfinia]
                    }
                },
                new NmTomo() {
                    @Override
                    List<Class<? extends Equipment>> getCompatibleEquipment() {
                        [OutsideGEInfinia]
                    }
                },
                new NmGatedTomo() {
                    @Override
                    List<Class<? extends Equipment>> getCompatibleEquipment() {
                        [OutsideGEInfinia]
                    }
                },
                new MyometrixResults() {
                    @Override
                    List<Class<? extends Equipment>> getCompatibleEquipment() {
                        [OutsideGEInfinia]
                    }
                }
        ]
    }

}
