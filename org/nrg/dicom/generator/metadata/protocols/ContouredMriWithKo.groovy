package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.ko.InstanceEnumerationKeyObjectNote
import org.nrg.dicom.generator.metadata.seriesTypes.mr.Localizer
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T1Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.mr.T2Weighted
import org.nrg.dicom.generator.metadata.seriesTypes.radiotherapy.RtstructSeriesType
import org.nrg.dicom.generator.util.RandomUtils

class ContouredMriWithKo extends Protocol {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'MRI W/ CONTRAST' : 40,
            "BRAIN MRI" : 40,
            'Brain' : 35,
            'MRI_AX_VIEWS_BRAIN' : 20,
            'DIAG MRI' : 10,
            'MRI BRAIN DIAGNOSTIC' : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Localizer(anatomicalPlane : AnatomicalPlane.TRANSVERSE),
                new T1Weighted(anatomicalPlane : AnatomicalPlane.TRANSVERSE),
                new T2Weighted(anatomicalPlane : AnatomicalPlane.TRANSVERSE),
                new RtstructSeriesType('T1'),
                new InstanceEnumerationKeyObjectNote()
        ]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [BodyPart.BRAIN]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

}
