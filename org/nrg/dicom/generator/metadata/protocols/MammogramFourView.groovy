package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.Laterality
import org.nrg.dicom.generator.metadata.enums.contextGroups.MammographyView
import org.nrg.dicom.generator.metadata.seriesTypes.mg.DigitalMammographyXRayForPresentation
import org.nrg.dicom.generator.util.RandomUtils

class MammogramFourView extends Mammogram {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'Screening Mamm Bilat' : 46,
            'SCREEN MAM-DIGITAL BILATERAL' : 30,
            'SCREENING MAMM BI' : 29,
            'Screening Mamm Bi' : 26,
            'DIGITAL SCREENING MAMM BILATERAL' : 11,
            'Diag Mammogram Bilateral' : 10,
            'DIGITAL MAMMOGRAM,BILATERAL' : 10
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new DigitalMammographyXRayForPresentation(Laterality.RIGHT, MammographyView.CC),
                new DigitalMammographyXRayForPresentation(Laterality.LEFT, MammographyView.CC),
                new DigitalMammographyXRayForPresentation(Laterality.RIGHT, MammographyView.MLO),
                new DigitalMammographyXRayForPresentation(Laterality.LEFT, MammographyView.MLO)
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        studyDescriptionRandomizer.sample()
    }

}
