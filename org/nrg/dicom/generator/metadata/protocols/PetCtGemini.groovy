package org.nrg.dicom.generator.metadata.protocols

import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.seriesTypes.ct.CtWithAttenuationCorrection
import org.nrg.dicom.generator.metadata.seriesTypes.ct.Topogram
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtNonAttenuationCorrected
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtWithAttenuationCorrection

class PetCtGemini extends PetCt {

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [
                new Topogram(),
                new CtWithAttenuationCorrection(),
                new PtWithAttenuationCorrection(),
                new PtNonAttenuationCorrected()
        ]
    }

}
