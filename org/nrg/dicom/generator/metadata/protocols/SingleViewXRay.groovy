package org.nrg.dicom.generator.metadata.protocols

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.seriesTypes.dx.DigitalXRayForPresentation
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class SingleViewXRay extends Protocol {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("XR ${BODYPART} 1V".toString()) : 100,
            (BODYPART) : 50
    ])

    @Override
    List<SeriesType> getAllSeriesTypes() {
        [new DigitalXRayForPresentation()]
    }

    @Override
    List<BodyPart> getApplicableBodyParts() {
        [
                BodyPart.ABDOMEN,
                BodyPart.CERVICAL_SPINE,
                BodyPart.CHEST,
                BodyPart.SKULL,
                BodyPart.SPINE,
                BodyPart.THORACIC_SPINE
        ]
    }

    @Override
    String getStudyDescription(Equipment scanner, BodyPart bodyPart) {
        randomizeWithBodyPart(randomizer, bodyPart)
    }

}
