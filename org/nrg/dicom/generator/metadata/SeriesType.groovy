package org.nrg.dicom.generator.metadata

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.Laterality
import org.nrg.dicom.generator.metadata.enums.SeriesBundling
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.instance.SopCommonModule
import org.nrg.dicom.generator.metadata.module.series.GeneralSeriesModule
import org.nrg.dicom.generator.util.StringReplacements

abstract class SeriesType {

    SpecificationParameters specificationParameters

    abstract String getModality()

    abstract String getSopClassUid() // We're not going to consider weird data that's technically compliant where there are multiple SOP classes within a series

    abstract String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined)

    abstract List<Class<? extends Equipment>> getCompatibleEquipment()

    abstract ImageType getImageType(Equipment equipment)

    final List<Instance> produceInstances(Patient patient, Study study, Equipment equipment, Series series) {
        specificationParameters.createFullSopInstances ? createCustomizedInstances(patient, study, equipment, series) : createGenericInstances(patient, study, equipment, series)
    }

    final List<Instance> createGenericInstances(Patient patient, Study study, Equipment equipment, Series series) {
        final Instance instance = new Instance()
        collectModules().each { module ->
            (module as InstanceLevelModule).apply(specificationParameters, patient, study, equipment, series, instance)
        }
        [instance]
    }

    List<Instance> createCustomizedInstances(Patient patient, Study study, Equipment equipment, Series series) {
        createGenericInstances(patient, study, equipment, series)
    }

    List<SeriesLevelModule> allSeriesModules() {
        [new GeneralSeriesModule()] + additionalSeriesModules() as List<SeriesLevelModule>
    }

    List<SeriesLevelModule> additionalSeriesModules() {
        []
    }

    List<InstanceLevelModule> additionalInstanceModules() {
        []
    }

    Class<? extends Series> seriesClass() {
        Series
    }

    SeriesBundling seriesBundling() {
        SeriesBundling.PRIMARY
    }

    Laterality laterality() {
        null
    }

    protected String replaceBodyPart(String baseString, BodyPart bodyPartExamined) {
        baseString.replace(StringReplacements.BODYPART, bodyPartExamined.dicomRepresentation).replace('WHOLEBODY', 'WB')
    }

    protected List<InstanceLevelModule> collectModules() {
        final List<InstanceLevelModule> additional = additionalInstanceModules()
        if (!specificationParameters.includePixelData) {
            additional.removeIf { module ->
                module instanceof ImagePixelModule
            }
        }
        [new SopCommonModule()] + additional as List<InstanceLevelModule>
    }

}
