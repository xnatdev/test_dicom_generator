package org.nrg.dicom.generator.metadata

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.PersonName
import org.nrg.dicom.generator.util.RandomUtils

import static org.dcm4che3.data.PersonName.Component.*
import static org.dcm4che3.data.PersonName.Group.*

class Person {

    private static final EnumeratedDistribution<Boolean> capitalizationRandomizer = RandomUtils.setupWeightedLottery([(false) : 99, (true) : 1])
    private static final EnumeratedDistribution<Closure<String>> middleInitialRandomizer = RandomUtils.setupWeightedLottery([
            ({ name -> null }) : 990,
            ({ name -> name.charAt(0) }) : 5,
            ({ name -> "${name.charAt(0)}." }) : 3,
            ({ name -> name }) : 2
    ]) as EnumeratedDistribution<Closure>
    private static final EnumeratedDistribution<Closure<String>> titleRandomizer = RandomUtils.setupWeightedLottery([
            ({ suffix -> suffix }) : 90,
            ({ suffix -> suffix.replace('.', '') }) : 7,
            ({ suffix -> null }) : 3
    ]) as EnumeratedDistribution<Closure>

    String familyNameAlphabetic
    String givenNameAlphabetic
    String middleNameAlphabetic
    String prefixAlphabetic
    String suffixAlphabetic
    String familyNameTransliterated
    String givenNameTransliterated
    String middleNameTransliterated
    String prefixTransliterated
    String suffixTransliterated
    String familyNameIdeographic
    String givenNameIdeographic
    String familyNamePhonetic
    String givenNamePhonetic
    List<String> requiredCharacterSets = []

    Person familyName(String alphabetic) {
        familyNameAlphabetic = alphabetic
        this
    }

    Person givenName(String alphabetic) {
        givenNameAlphabetic = alphabetic
        this
    }

    Person middleName(String alphabetic) {
        middleNameAlphabetic = alphabetic
        this
    }

    Person suffix(String alphabetic) {
        suffixAlphabetic = alphabetic
        this
    }

    Person prefix(String alphabetic) {
        prefixAlphabetic = alphabetic
        this
    }

    Person familyName(String alphabetic, String ideographic, String phonetic) {
        familyNameAlphabetic = alphabetic
        familyNameIdeographic = ideographic
        familyNamePhonetic = phonetic
        this
    }

    Person givenName(String alphabetic, String ideographic, String phonetic) {
        givenNameAlphabetic = alphabetic
        givenNameIdeographic = ideographic
        givenNamePhonetic = phonetic
        this
    }

    Person familyNameTransliterated(String alphabetic, String transliterated) {
        setFamilyNameAlphabetic(alphabetic)
        setFamilyNameTransliterated(transliterated)
        this
    }

    Person givenNameTransliterated(String alphabetic, String transliterated) {
        setGivenNameAlphabetic(alphabetic)
        setGivenNameTransliterated(transliterated)
        this
    }

    String serializeToDicom(boolean allowNonlatin = false) {
        final PersonName personName = new PersonName()
        final Closure<String> coercionFunction = capitalizationRandomizer.sample() ? { String name -> name.toUpperCase() } : { String name -> name }

        personName.set(Alphabetic, FamilyName, coercionFunction(transliterate(familyNameAlphabetic, familyNameTransliterated, allowNonlatin)))
        personName.set(Alphabetic, GivenName, coercionFunction(transliterate(givenNameAlphabetic, givenNameTransliterated, allowNonlatin)))
        if (middleNameAlphabetic != null) {
            final String middleRepresentation = middleInitialRandomizer.sample()(transliterate(middleNameAlphabetic, middleNameTransliterated, allowNonlatin))
            if (middleRepresentation != null) {
                personName.set(Alphabetic, MiddleName, coercionFunction(middleRepresentation))
            }
        }
        if (suffixAlphabetic != null) {
            final String suffixRepresentation = titleRandomizer.sample()(transliterate(suffixAlphabetic, suffixTransliterated, allowNonlatin))
            if (suffixRepresentation != null) {
                personName.set(Alphabetic, NameSuffix, coercionFunction(suffixRepresentation))
            }
        }
        if (prefixAlphabetic != null) {
            final String prefixRepresentation = titleRandomizer.sample()(transliterate(prefixAlphabetic, prefixTransliterated, allowNonlatin))
            if (prefixRepresentation != null) {
                personName.set(Alphabetic, NamePrefix, coercionFunction(prefixRepresentation))
            }
        }
        if (allowNonlatin) {
            if (familyNameIdeographic != null) {
                personName.set(Ideographic, FamilyName, familyNameIdeographic)
            }
            if (givenNameIdeographic != null) {
                personName.set(Ideographic, GivenName, givenNameIdeographic)
            }
            if (familyNamePhonetic != null) {
                personName.set(Phonetic, FamilyName, familyNamePhonetic)
            }
            if (givenNamePhonetic != null) {
                personName.set(Phonetic, GivenName, givenNamePhonetic)
            }
        }

        personName.toString()
    }

    String serializeToInitials() {
        "${givenNameAlphabetic.charAt(0)}${middleNameAlphabetic?.charAt(0) ?: ''}${familyNameAlphabetic.charAt(0)}"
    }

    private static String transliterate(String alphabetic, String transliterated, boolean allowNonlatin) {
        (!allowNonlatin && transliterated != null) ? transliterated : alphabetic
    }

}
