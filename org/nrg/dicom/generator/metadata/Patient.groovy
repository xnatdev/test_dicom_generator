package org.nrg.dicom.generator.metadata

import com.fasterxml.jackson.annotation.JsonIgnore
import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.data.VR
import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.enums.EthnicGroup
import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.metadata.enums.Sex

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class Patient implements DicomEncoder {

    private static final LocalDate imagingDataEpoch = LocalDate.of(1994, 6, 12) // patient might have been born in 1930, but we didn't have DICOM-compliant MRI machines in the 1930s!

    Sex sex
    LocalDate dateOfBirth
    LocalTime timeOfBirth
    Person patientName
    String patientId
    EthnicGroup ethnicGroup
    List<Study> studies = []
    String patientInstanceUid // non-standard element
    @JsonIgnore LocalDate earliestAvailableStudyDate
    @JsonIgnore Nationality nationality
    @JsonIgnore double personalHeightMod
    @JsonIgnore double personalWeightMod

    Patient randomize(SpecificationParameters specificationParameters, double currentAverageStudiesPerPatient, long previouslyGeneratedSeries, long previouslyGeneratedStudies, List<SortableStudy> sortableStudies) {
        long numSeries = previouslyGeneratedSeries
        long numStudies = previouslyGeneratedStudies
        final LocalDate sixteenthBirthday = dateOfBirth.plusYears(16)
        patientInstanceUid = UIDUtils.createUID()
        earliestAvailableStudyDate = sixteenthBirthday.isAfter(imagingDataEpoch) ? sixteenthBirthday : imagingDataEpoch
        specificationParameters.chooseNumberOfStudies(currentAverageStudiesPerPatient).times {
            final Protocol selectedProtocol = specificationParameters.chooseProtocol(numSeries == 0 ? 0.0 : numSeries / numStudies, this)
            final Study study = new Study(protocol : selectedProtocol, patient : this).randomize(specificationParameters, selectedProtocol)
            if (sortableStudies != null) {
                sortableStudies << new SortableStudy(
                        patientInstanceUID : patientInstanceUid,
                        studyInstanceUID : study.studyInstanceUid,
                        studyDateTime : LocalDateTime.of(study.studyDate, study.studyTime)
                )
            }
            studies << study
            numStudies++
            numSeries += study.series.size()
        }
        this
    }

    void encode(Attributes attributes) {
        attributes.setString(Tag.PatientSex, VR.CS, sex.dicomRepresentation)
        setDate(attributes, Tag.PatientBirthDate, dateOfBirth)
        setTime(attributes, Tag.PatientBirthTime, timeOfBirth)
        attributes.setString(Tag.PatientID, VR.LO, patientId)
    }

}
