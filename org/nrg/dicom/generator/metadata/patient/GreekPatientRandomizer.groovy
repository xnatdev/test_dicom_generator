package org.nrg.dicom.generator.metadata.patient

import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.Nationality

class GreekPatientRandomizer extends PatientRandomizer {

    @Override
    void randomize(Patient patient) {
        assignRandomPersonName(patient, Nationality.GREEK)
    }

}
