package org.nrg.dicom.generator.metadata.patient

import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.Nationality

class DefaultPatientRandomizer extends PatientRandomizer {

    @Override
    void randomize(Patient patient) {
        assignRandomEthnicGroup(patient)
        assignRandomPersonName(patient, Nationality.AMERICAN)
    }

}
