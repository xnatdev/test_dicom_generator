package org.nrg.dicom.generator.metadata.patient

import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.Nationality

class JapanesePatientRandomizer extends PatientRandomizer {

    @Override
    void randomize(Patient patient) {
        assignRandomPersonName(patient, Nationality.JAPANESE)
    }

}
