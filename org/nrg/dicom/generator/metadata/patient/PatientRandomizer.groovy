package org.nrg.dicom.generator.metadata.patient

import org.apache.commons.math3.distribution.BetaDistribution
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.distribution.NormalDistribution
import org.apache.commons.math3.distribution.RealDistribution
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.EthnicGroup
import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.metadata.module.patient.PatientModule
import org.nrg.dicom.generator.util.RandomUtils

abstract class PatientRandomizer {

    public static final EnumeratedDistribution<EthnicGroup> ethnicGroupDistribution = initializeEthnicRandomizer()
    public static final RealDistribution heightModDistribution = new NormalDistribution(0, 1)
    public static final RealDistribution weightModDistribution = new BetaDistribution(2, 5)

    Patient createPatient(SpecificationParameters specificationParameters) {
        final Patient patient = new Patient()
        new PatientModule().apply(specificationParameters, patient)
        randomize(patient)
        assignRandomPhysique(patient)
        patient
    }

    abstract void randomize(Patient patient)

    void assignRandomPersonName(Patient patient, Nationality nationality) {
        patient.setPatientName(nationality.generateRandomPerson(patient.sex))
        patient.setNationality(nationality)
    }

    void assignRandomEthnicGroup(Patient patient) {
        patient.setEthnicGroup(ethnicGroupDistribution.sample())
    }

    void assignRandomPhysique(Patient patient) {
        while (true) {
            final double sample = heightModDistribution.sample()
            if (Math.abs(sample) <= 3) {
                patient.setPersonalHeightMod(sample)
                break
            }
        }
        patient.setPersonalWeightMod(10 * weightModDistribution.sample() - 2) // apply affine transformation to coerce value from [0, 1] to [-2, 8]
    }

    private static EnumeratedDistribution<EthnicGroup> initializeEthnicRandomizer() {
        RandomUtils.setupWeightedLottery([
                (EthnicGroup.WHITE) : 40,
                (EthnicGroup.NON_HISPANIC) : 25,
                (EthnicGroup.BLACK) : 15,
                (EthnicGroup.HISPANIC) : 7,
                (EthnicGroup.ASIAN) : 5,
                (EthnicGroup.OTHER) : 4,
                (EthnicGroup.MULTI_RACIAL) : 3,
                (EthnicGroup.AMERICAN_INDIAN) : 1
        ])
    }

}