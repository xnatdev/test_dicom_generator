package org.nrg.dicom.generator.metadata.sequence

import org.dcm4che3.data.Attributes

interface SequenceElement {

    void addToAttributes(Attributes attributes)

}