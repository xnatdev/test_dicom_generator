package org.nrg.dicom.generator.metadata.seriesTypes.radiotherapy

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Randomizeable
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.SeriesBundling
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.series.RtSeriesModule
import org.nrg.dicom.generator.metadata.scanners.AdacPinnacle3
import org.nrg.dicom.generator.metadata.series.RtstructSeries
import org.nrg.dicom.generator.util.RandomUtils
import org.nrg.dicom.generator.util.StringReplacements

class RtstructSeriesType extends SeriesType implements Randomizeable {

    private final String referencedImageType
    private static final String TYPE_KEY = '%TYPE%'
    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            ("RTstruct_${TYPE_KEY}".toString()) : 40,
            'RT Structures' : 30,
            ("${StringReplacements.BODYPART}".toString()) : 20,
            'RT Data' : 10
    ])

    RtstructSeriesType(String type) {
        referencedImageType = type
    }

    @Override
    String getModality() {
        'RTSTRUCT'
    }

    @Override
    String getSopClassUid() {
        UID.RTStructureSetStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        randomizeWithBodyPart(seriesDescriptionRandomizer, bodyPartExamined).replace(TYPE_KEY, referencedImageType)
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [AdacPinnacle3]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        null
    }

    @Override
    List<SeriesLevelModule> allSeriesModules() {
        [new RtSeriesModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        RtstructSeries
    }

    @Override
    SeriesBundling seriesBundling() {
        SeriesBundling.RTSTRUCT
    }

}
