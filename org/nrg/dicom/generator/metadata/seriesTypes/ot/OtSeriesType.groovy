package org.nrg.dicom.generator.metadata.seriesTypes.ot

import org.nrg.dicom.generator.metadata.SeriesType

abstract class OtSeriesType extends SeriesType {

    @Override
    String getModality() {
        'OT'
    }
    
}
