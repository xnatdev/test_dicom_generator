package org.nrg.dicom.generator.metadata.seriesTypes.pt

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.PetImageCorrection
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue1
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue2
import org.nrg.dicom.generator.metadata.enums.PetUnit
import org.nrg.dicom.generator.metadata.scanners.PetScanner
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class PtWithAttenuationCorrection extends PtSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("PET ${BODYPART} (AC)".toString()) : 50,
            ("PET ${BODYPART}".toString()) : 30,
            'PT_AC' : 20
    ])

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        replaceBodyPart(randomizer.sample(), bodyPartExamined)
    }

    @Override
    boolean isAttenuationCorrected() {
        true
    }

    @Override
    PetSeriesTypeValue1 getSeriesTypeValue1() {
        PetSeriesTypeValue1.WHOLE_BODY
    }

    @Override
    PetSeriesTypeValue2 getSeriesTypeValue2() {
        PetSeriesTypeValue2.IMAGE
    }

    @Override
    List<PetImageCorrection> getImageCorrections(Series series) {
        (series.scanner as PetScanner).attenuatedCorrections
    }

}
