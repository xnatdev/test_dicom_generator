package org.nrg.dicom.generator.metadata.seriesTypes.pt

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.PetImageCorrection
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue1
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue2
import org.nrg.dicom.generator.metadata.enums.PetUnit
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographmMR
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class MracPetWithAttenuationCorrection extends PtSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("${BODYPART}_MRAC_PET_AC Images".toString()) : 50,
            ("${BODYPART}_MRAC_PET_AC".toString()) : 30,
            'MRAC_PET_AC' : 30
    ])

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        replaceBodyPart(randomizer.sample(), bodyPartExamined)
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().addValue('STATIC').addValue('AC')
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SiemensBiographmMR]
    }

    @Override
    boolean isAttenuationCorrected() {
        true
    }

    @Override
    PetSeriesTypeValue1 getSeriesTypeValue1() {
        PetSeriesTypeValue1.WHOLE_BODY
    }

    @Override
    PetSeriesTypeValue2 getSeriesTypeValue2() {
        PetSeriesTypeValue2.IMAGE
    }

    @Override
    List<PetImageCorrection> getImageCorrections(Series series) {
        [
                PetImageCorrection.NORM,
                PetImageCorrection.DTIM,
                PetImageCorrection.ATTN,
                PetImageCorrection._3SCAT,
                PetImageCorrection.DECY,
                PetImageCorrection.FLEN,
                PetImageCorrection.RANSM,
                PetImageCorrection.XYSM,
                PetImageCorrection.ZSM
        ]
    }

}
