package org.nrg.dicom.generator.metadata.seriesTypes.pt

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.PetImageCorrection
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue1
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue2
import org.nrg.dicom.generator.metadata.enums.PetUnit
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.series.NmPetOrientationModule
import org.nrg.dicom.generator.metadata.module.series.PetIsotopeModule
import org.nrg.dicom.generator.metadata.module.series.PetSeriesModule
import org.nrg.dicom.generator.metadata.scanners.PhilipsGeminiR35
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographTruePoint64
import org.nrg.dicom.generator.metadata.series.PtSeries

abstract class PtSeriesType extends SeriesType {

    @Override
    String getModality() {
        'PT'
    }

    @Override
    String getSopClassUid() {
        UID.PositronEmissionTomographyImageStorage
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SiemensBiographTruePoint64, PhilipsGeminiR35]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType()
    }

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [
                new PetSeriesModule(),
                new PetIsotopeModule(),
                new NmPetOrientationModule()
        ]
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        PtSeries
    }

    abstract boolean isAttenuationCorrected()
    abstract PetSeriesTypeValue1 getSeriesTypeValue1()
    abstract PetSeriesTypeValue2 getSeriesTypeValue2()
    abstract List<PetImageCorrection> getImageCorrections(Series series)

}
