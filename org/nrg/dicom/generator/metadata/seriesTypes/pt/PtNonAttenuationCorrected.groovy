package org.nrg.dicom.generator.metadata.seriesTypes.pt

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.PetImageCorrection
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue1
import org.nrg.dicom.generator.metadata.enums.PetSeriesTypeValue2
import org.nrg.dicom.generator.metadata.enums.PetUnit
import org.nrg.dicom.generator.metadata.scanners.PetScanner
import org.nrg.dicom.generator.util.RandomUtils

import static org.nrg.dicom.generator.util.StringReplacements.BODYPART

class PtNonAttenuationCorrected extends PtSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            ("PET ${BODYPART} (NAC)".toString()) : 50,
            'PT_NAC' : 30,
            ("PET ${BODYPART} Uncorrected".toString()) : 20
    ])

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        replaceBodyPart(randomizer.sample(), bodyPartExamined)
    }

    @Override
    boolean isAttenuationCorrected() {
        false
    }

    @Override
    PetSeriesTypeValue1 getSeriesTypeValue1() {
        PetSeriesTypeValue1.DYNAMIC
    }

    @Override
    PetSeriesTypeValue2 getSeriesTypeValue2() {
        PetSeriesTypeValue2.IMAGE
    }

    @Override
    List<PetImageCorrection> getImageCorrections(Series series) {
        (series.scanner as PetScanner).nonAttenuatedCorrections
    }

}
