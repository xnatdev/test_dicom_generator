package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.util.RandomUtils

class BloodOxygenLevelDependentImage extends MrSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'fMRI' : 30,
            'fMRI_BOLD_%PLANE%' : 30,
            'BOLD' : 20
    ])

    @Override
    EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner) {
        randomizer
    }

}
