package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographmMR
import org.nrg.dicom.generator.util.RandomUtils

class Localizer extends MrSeriesType {

    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'localizer_%PLANE%' : 30,
            '%PLANE% LOC' : 20,
            '%PLANE% Scout' : 20,
            'scout_%PLANE%' : 15
    ])

    @Override
    EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner) {
        seriesDescriptionRandomizer
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        super.compatibleEquipment + [SiemensBiographmMR] as List<Class<? extends Equipment>>
    }

}
