package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographmMR
import org.nrg.dicom.generator.util.RandomUtils

class ThreePlaneLocalizer extends MrSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            '3 Plane Localizer' : 100,
            '3 PLANE LOC' : 40,
            '3 Plane Localizer_ND' : 35,
            '3-plane localizer' : 30,
            '3Plane Loc' : 15,
            'localizer 3 plane' : 5
    ])

    @Override
    EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner) {
        randomizer
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().addValue('M').addValue('ND')
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        super.compatibleEquipment + [SiemensBiographmMR] as List<Class<? extends Equipment>>
    }

}
