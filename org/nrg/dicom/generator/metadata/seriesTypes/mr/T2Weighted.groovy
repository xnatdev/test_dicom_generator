package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.util.RandomUtils

class T2Weighted extends MrSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            't2w_%PLANE%' : 30,
            '%PLANE% T2w' : 30,
            'T2W %PLANE%' : 20,
            'T2W_%PLANE%' : 10
    ])

    @Override
    EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner) {
        randomizer
    }

}
