package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.AnatomicalPlane
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.scanners.GEOptimaMR450w
import org.nrg.dicom.generator.metadata.scanners.PhilipsAchieva
import org.nrg.dicom.generator.metadata.scanners.SiemensAvanto
import org.nrg.dicom.generator.util.StringReplacements

abstract class MrSeriesType extends SeriesType {

    AnatomicalPlane anatomicalPlane

    @Override
    String getModality() {
        'MR'
    }

    @Override
    String getSopClassUid() {
        UID.MRImageStorage
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [
                PhilipsAchieva,
                SiemensAvanto,
                GEOptimaMR450w
        ]
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        getSeriesDescriptionRandomizer(scanner).
                sample().
                replace('%PLANE%', anatomicalPlane ? anatomicalPlane.sampleEncodedValue() : '').
                replace(StringReplacements.BODYPART, bodyPartExamined.dicomRepresentation)
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType() // TODO
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    abstract EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner)

}
