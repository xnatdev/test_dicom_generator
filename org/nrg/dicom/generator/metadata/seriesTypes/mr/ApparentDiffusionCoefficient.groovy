package org.nrg.dicom.generator.metadata.seriesTypes.mr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.util.RandomUtils

class ApparentDiffusionCoefficient extends MrSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'ADC' : 7,
            'Apparent Diffusion Coefficient (mm?/s)' : 3
    ])

    @Override
    EnumeratedDistribution<String> getSeriesDescriptionRandomizer(Equipment scanner) {
        randomizer
    }

}
