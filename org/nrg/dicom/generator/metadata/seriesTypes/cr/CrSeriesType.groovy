package org.nrg.dicom.generator.metadata.seriesTypes.cr

import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.XRayViewPosition
import org.nrg.dicom.generator.metadata.series.CrSeries

abstract class CrSeriesType extends SeriesType {

    @Override
    String getModality() {
        'CR'
    }

    @Override
    Class<? extends Series> seriesClass() {
        CrSeries
    }

    abstract XRayViewPosition getViewPosition()

}
