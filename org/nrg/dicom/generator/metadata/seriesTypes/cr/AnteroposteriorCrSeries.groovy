package org.nrg.dicom.generator.metadata.seriesTypes.cr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.XRayViewPosition
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.series.CrSeriesModule
import org.nrg.dicom.generator.metadata.scanners.PhilipsMobileDiagnostwDR
import org.nrg.dicom.generator.util.RandomUtils

class AnteroposteriorCrSeries extends CrSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'AP Grid' : 50,
            'AP Portrait' : 30,
            'AP Landscape' : 20
    ])

    @Override
    String getSopClassUid() {
        UID.ComputedRadiographyImageStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        randomizer.sample()
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [PhilipsMobileDiagnostwDR]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType()
    }

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [new CrSeriesModule()]
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    @Override
    XRayViewPosition getViewPosition() {
        XRayViewPosition.AP
    }

}
