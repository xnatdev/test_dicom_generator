package org.nrg.dicom.generator.metadata.seriesTypes.ct

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.scanners.CtScanner
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.util.RandomUtils

class CtWithAttenuationCorrection extends CtSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'CT_AC' : 70,
            'CTAC' : 30
    ])

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        randomizer.sample()
    }

    @Override
    ImageType resolveImageType(CtScanner equipment) {
        equipment.getAttenuationCorrectedCtImageType()
    }

}
