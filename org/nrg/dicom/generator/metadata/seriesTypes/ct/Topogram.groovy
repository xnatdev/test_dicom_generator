package org.nrg.dicom.generator.metadata.seriesTypes.ct

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.scanners.CtScanner
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.util.RandomUtils

class Topogram extends CtSeriesType {

    private static final EnumeratedDistribution<Double> sliceThicknesses = RandomUtils.setupWeightedLottery([
            (0.6) : 30,
            (1.0) : 50,
            (3.0) : 50,
            (5.0) : 80
    ]) as EnumeratedDistribution<Double>

    private static final EnumeratedDistribution<String> kernels = RandomUtils.setupWeightedLottery([
            'T80f' : 50,
            'T80s' : 50
    ])

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        "Topogram ${sliceThicknesses.sample()} ${kernels.sample()}"
    }

    @Override
    ImageType resolveImageType(CtScanner equipment) {
        equipment.getTopogramImageType()
    }

}
