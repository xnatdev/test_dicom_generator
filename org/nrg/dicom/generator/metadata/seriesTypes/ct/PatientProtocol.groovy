package org.nrg.dicom.generator.metadata.seriesTypes.ct

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.scanners.CtScanner
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographTruePoint64

class PatientProtocol extends CtSeriesType {

    @Override
    String getSopClassUid() {
        UID.SecondaryCaptureImageStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        'Patient Protocol'
    }

    @Override
    ImageType resolveImageType(CtScanner equipment) {
        new ImageType().derived().secondary().addValue('OTHER').addValue('CT_SOM5 PROT')
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SiemensBiographTruePoint64]
    }

}
