package org.nrg.dicom.generator.metadata.seriesTypes.ct

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.scanners.CtScanner
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.scanners.PhilipsGeminiR35
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographTruePoint64

abstract class CtSeriesType extends SeriesType {

    @Override
    String getModality() {
        'CT'
    }

    @Override
    String getSopClassUid() {
        UID.CTImageStorage
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SiemensBiographTruePoint64, PhilipsGeminiR35]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        resolveImageType(equipment as CtScanner)
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    abstract ImageType resolveImageType(CtScanner equipment)

}
