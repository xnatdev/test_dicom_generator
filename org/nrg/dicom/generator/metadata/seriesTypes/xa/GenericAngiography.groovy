package org.nrg.dicom.generator.metadata.seriesTypes.xa

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.scanners.XaScanner
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.scanners.PhilipsAlluraXper
import org.nrg.dicom.generator.metadata.scanners.SiemensAxiomArtis

class GenericAngiography extends XaSeriesType {

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        (scanner as XaScanner).seriesDescription
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [PhilipsAlluraXper, SiemensAxiomArtis]
    }

    @Override
    XaImageType getXaImageType(XaScanner scanner) {
        scanner.imageType
    }

}
