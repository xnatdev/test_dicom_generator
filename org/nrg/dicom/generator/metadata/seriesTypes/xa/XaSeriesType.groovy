package org.nrg.dicom.generator.metadata.seriesTypes.xa

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.scanners.XaScanner

abstract class XaSeriesType extends SeriesType {

    @Override
    String getModality() {
        'XA'
    }

    @Override
    String getSopClassUid() {
        UID.XRayAngiographicImageStorage
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        getXaImageType(equipment as XaScanner).resolve()
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    abstract XaImageType getXaImageType(XaScanner scanner)

}
