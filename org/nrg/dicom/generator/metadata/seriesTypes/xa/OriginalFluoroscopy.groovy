package org.nrg.dicom.generator.metadata.seriesTypes.xa

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.scanners.XaScanner
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.scanners.PhilipsAlluraXper

class OriginalFluoroscopy extends XaSeriesType {

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        'Fluoroscopy'
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [PhilipsAlluraXper]
    }

    @Override
    XaImageType getXaImageType(XaScanner scanner) {
        new XaImageType().addValue('SINGLE A')
    }

}
