package org.nrg.dicom.generator.metadata.seriesTypes.dx

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.series.DxSeriesModule
import org.nrg.dicom.generator.metadata.scanners.GEDiscoveryXR656
import org.nrg.dicom.generator.metadata.series.DxSeries

import java.util.concurrent.ThreadLocalRandom

class DigitalXRayForPresentation extends SeriesType {

    @Override
    String getModality() {
        'DX'
    }

    @Override
    String getSopClassUid() {
        UID.DigitalXRayImageStorageForPresentation
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        ThreadLocalRandom.current().nextBoolean() ? bodyPartExamined.dicomRepresentation : bodyPartExamined.code.codeMeaning
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [GEDiscoveryXR656]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().addValue('')
    }

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [new DxSeriesModule()]
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        DxSeries
    }

}
