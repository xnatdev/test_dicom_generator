package org.nrg.dicom.generator.metadata.seriesTypes.sr

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.scanners.SiemensBiographmMR

class PhoenixZIPReport extends SrSeriesType {

    @Override
    String getSopClassUid() {
        UID.EnhancedSRStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        'PhoenixZIPReport'
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SiemensBiographmMR]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().addValue('OTHER').addValue('CSA REPORT')
    }

}
