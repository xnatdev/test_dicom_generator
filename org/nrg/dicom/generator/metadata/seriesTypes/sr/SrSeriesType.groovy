package org.nrg.dicom.generator.metadata.seriesTypes.sr

import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.series.SrDocumentSeriesModule
import org.nrg.dicom.generator.metadata.series.SrSeries

abstract class SrSeriesType extends SeriesType {

    @Override
    String getModality() {
        'SR'
    }

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [new SrDocumentSeriesModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        SrSeries
    }

}
