package org.nrg.dicom.generator.metadata.seriesTypes.nm

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.NmScanType

class MyometrixResults extends NmSeriesType {

    @Override
    NmImageType getImageType() {
        new NmImageType().scanType(NmScanType.RECON_TOMO).derived()
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        'Myometrix Results'
    }

}
