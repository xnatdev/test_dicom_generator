package org.nrg.dicom.generator.metadata.seriesTypes.nm

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.NmScanType
import org.nrg.dicom.generator.util.RandomUtils

class RestingNm extends NmSeriesType {

    private static final EnumeratedDistribution<String> randomizer = RandomUtils.setupWeightedLottery([
            'REST' : 70,
            'Resting' : 30
    ])

    @Override
    NmImageType getImageType() {
        new NmImageType().scanType(NmScanType.TOMO)
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        randomizer.sample()
    }

}
