package org.nrg.dicom.generator.metadata.seriesTypes.nm

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.series.NmPetOrientationModule
import org.nrg.dicom.generator.metadata.scanners.GEInfinia
import org.nrg.dicom.generator.metadata.series.NmSeries

abstract class NmSeriesType extends SeriesType {

    @Override
    String getModality() {
        'NM'
    }

    @Override
    String getSopClassUid() {
        UID.NuclearMedicineImageStorage
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [GEInfinia]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        getImageType().resolve()
    }

    abstract NmImageType getImageType()

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [new NmPetOrientationModule()]
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        NmSeries
    }

}
