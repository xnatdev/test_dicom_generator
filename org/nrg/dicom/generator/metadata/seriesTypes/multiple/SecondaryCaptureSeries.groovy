package org.nrg.dicom.generator.metadata.seriesTypes.multiple

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.scanners.LumisysLS75

abstract class SecondaryCaptureSeries extends SeriesType {

    @Override
    String getSopClassUid() {
        UID.SecondaryCaptureImageStorage
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [LumisysLS75]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().derived().secondary().addValue('UNCOMPRESSED')
    }

}
