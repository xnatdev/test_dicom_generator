package org.nrg.dicom.generator.metadata.seriesTypes.multiple

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.util.RandomUtils

class SecondaryCaptureXRaySeries extends SecondaryCaptureSeries {

    private static final EnumeratedDistribution<String> xnatCompliantModalityRandomizer = RandomUtils.setupWeightedLottery([
            'DX' : 60,
            'CR' : 30,
            'OT' : 10
    ])
    private static final EnumeratedDistribution<String> generalModalityRandomizer = RandomUtils.setupWeightedLottery([
            'DX' : 40,
            'CR' : 25,
            'OT' : 15,
            'SC' : 15,
            'XR' : 5
    ])
    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'Digitized Film' : 40,
            'Outside Image' : 30,
            'Scanned XRay' : 30
    ])

    @Override
    String getModality() {
        (specificationParameters.requireXnatCompatibility ? xnatCompliantModalityRandomizer : generalModalityRandomizer).sample()
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        seriesDescriptionRandomizer.sample()
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

}
