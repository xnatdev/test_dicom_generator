package org.nrg.dicom.generator.metadata.seriesTypes.multiple

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.util.RandomUtils

class SecondaryCaptureMammogramSeries extends SecondaryCaptureSeries {

    private static final EnumeratedDistribution<String> xnatCompliantModalityRandomizer = RandomUtils.setupWeightedLottery([
            'MG' : 90,
            'OT' : 10
    ])
    private static final EnumeratedDistribution<String> generalModalityRandomizer = RandomUtils.setupWeightedLottery([
            'MG' : 80,
            'OT' : 10,
            'SC' : 10
    ])
    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'Digitized Film' : 30,
            'Outside Image' : 30,
            'Scanned Mammogram' : 30,
            'MAMM 1 VIEW' : 10
    ])

    @Override
    String getModality() {
        (specificationParameters.requireXnatCompatibility ? xnatCompliantModalityRandomizer : generalModalityRandomizer).sample()
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        seriesDescriptionRandomizer.sample()
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

}
