package org.nrg.dicom.generator.metadata.seriesTypes.ko

import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.SeriesBundling
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.series.KeyObjectDocumentSeriesModule
import org.nrg.dicom.generator.metadata.scanners.SwiftPacs

class InstanceEnumerationKeyObjectNote extends KoSeriesType {

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        'Instance Enumeration'
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [SwiftPacs]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        null
    }

    @Override
    List<SeriesLevelModule> allSeriesModules() {
        [new KeyObjectDocumentSeriesModule()]
    }

    @Override
    SeriesBundling seriesBundling() {
        SeriesBundling.KO
    }

}
