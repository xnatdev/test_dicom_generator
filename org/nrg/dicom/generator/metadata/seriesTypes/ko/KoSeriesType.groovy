package org.nrg.dicom.generator.metadata.seriesTypes.ko

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.series.KoSeries

abstract class KoSeriesType extends SeriesType {

    @Override
    String getModality() {
        'KO'
    }

    @Override
    String getSopClassUid() {
        UID.KeyObjectSelectionDocumentStorage
    }

    @Override
    Class<? extends Series> seriesClass() {
        KoSeries
    }

}
