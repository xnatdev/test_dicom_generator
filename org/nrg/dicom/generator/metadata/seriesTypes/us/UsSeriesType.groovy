package org.nrg.dicom.generator.metadata.seriesTypes.us

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.scanners.AcusonSequoia
import org.nrg.dicom.generator.metadata.scanners.PhilipsiE33

class UsSeriesType extends SeriesType {

    @Override
    String getModality() {
        'US'
    }

    @Override
    String getSopClassUid() {
        UID.UltrasoundImageStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        null // not a mistake, this element is often left out of ultrasounds
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [AcusonSequoia, PhilipsiE33]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().addValue('').addValue('0001')
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

}
