package org.nrg.dicom.generator.metadata.seriesTypes.mg

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.Laterality
import org.nrg.dicom.generator.metadata.enums.contextGroups.MammographyView
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.module.instance.ImagePixelModule
import org.nrg.dicom.generator.metadata.module.series.DxSeriesModule
import org.nrg.dicom.generator.metadata.module.series.MammographySeriesModule
import org.nrg.dicom.generator.metadata.module.series.MgSeries
import org.nrg.dicom.generator.metadata.scanners.LoradSelenia

class DigitalMammographyXRayForPresentation extends SeriesType {

    final Laterality laterality
    final MammographyView viewPosition
    private final String description

    DigitalMammographyXRayForPresentation(Laterality laterality, MammographyView viewPosition) {
        this.laterality = laterality
        this.viewPosition = viewPosition
        description = "${laterality.dicomRepresentation} ${viewPosition.dicomRepresentation}"
    }

    @Override
    String getModality() {
        'MG'
    }

    @Override
    String getSopClassUid() {
        UID.DigitalMammographyXRayImageStorageForPresentation
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        description
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [LoradSelenia]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        new ImageType().derived().secondary().addValue('') // PS3.3 C.8.11.7.1.4
    }

    @Override
    Laterality laterality() {
        laterality
    }

    @Override
    List<SeriesLevelModule> additionalSeriesModules() {
        [new DxSeriesModule(), new MammographySeriesModule()]
    }

    @Override
    List<InstanceLevelModule> additionalInstanceModules() {
        [new ImagePixelModule()]
    }

    @Override
    Class<? extends Series> seriesClass() {
        MgSeries
    }

}
