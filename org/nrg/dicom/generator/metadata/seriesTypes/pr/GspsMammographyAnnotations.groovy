package org.nrg.dicom.generator.metadata.seriesTypes.pr

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.enums.SeriesBundling
import org.nrg.dicom.generator.metadata.scanners.HologicSecurViewDx
import org.nrg.dicom.generator.util.RandomUtils

class GspsMammographyAnnotations extends PrSeriesType {

    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'Presentation State' : 40,
            'MASS ANNOTATIONS' : 30,
            'Annotation - Breast' : 30,
            'Measurements' : 10
    ])

    @Override
    String getSopClassUid() {
        UID.GrayscaleSoftcopyPresentationStateStorage
    }

    @Override
    String getSeriesDescription(Equipment scanner, BodyPart bodyPartExamined) {
        seriesDescriptionRandomizer.sample()
    }

    @Override
    List<Class<? extends Equipment>> getCompatibleEquipment() {
        [HologicSecurViewDx]
    }

    @Override
    ImageType getImageType(Equipment equipment) {
        null
    }

    @Override
    SeriesBundling seriesBundling() {
        SeriesBundling.PR_MG
    }

}
