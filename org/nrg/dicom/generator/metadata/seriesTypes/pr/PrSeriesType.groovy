package org.nrg.dicom.generator.metadata.seriesTypes.pr

import org.nrg.dicom.generator.metadata.SeriesType

abstract class PrSeriesType extends SeriesType {

    @Override
    String getModality() {
        'PR'
    }

}
