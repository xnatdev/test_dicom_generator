package org.nrg.dicom.generator.metadata

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.util.StringReplacements

trait Randomizeable {

    String randomizeWithBodyPart(EnumeratedDistribution<String> enumeratedDistribution, BodyPart bodyPart) {
        enumeratedDistribution.sample().replace(StringReplacements.BODYPART, bodyPart.dicomRepresentation)
    }

}