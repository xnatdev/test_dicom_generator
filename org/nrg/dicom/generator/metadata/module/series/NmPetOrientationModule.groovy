package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.contextGroups.PatientEquipmentRelationship
import org.nrg.dicom.generator.metadata.enums.PatientOrientation
import org.nrg.dicom.generator.metadata.enums.contextGroups.PatientOrientationModifier
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.series.SupportsNmPetPatientOrientation

class NmPetOrientationModule implements SeriesLevelModule<Series> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, Series series) {
        final SupportsNmPetPatientOrientation castedSeries = series as SupportsNmPetPatientOrientation
        castedSeries.setPatientOrientationSeqValue(PatientOrientation.RECUMBENT)
        castedSeries.setPatientOrientationModifierSeqValue(PatientOrientationModifier.SUPINE)
        castedSeries.setPatientGantryRelationshipSeqValue(PatientEquipmentRelationship.HEADFIRST)
    }

}
