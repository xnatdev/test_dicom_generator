package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.series.RtSeries

class RtSeriesModule implements SeriesLevelModule<RtSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, RtSeries series) {
        GeneralSeriesModule.setModality(series)
        GeneralSeriesModule.setSeriesInstanceUid(series)
        GeneralSeriesModule.setSeriesNumber(series)
        GeneralSeriesModule.setSeriesDate(study, series)
        GeneralSeriesModule.setSeriesTime(study, series)
        GeneralSeriesModule.setSeriesDescription(study, series)
        series.setOperatorsName(study.operatorMap?.get(equipment) ?: ['']) // type 3 in General Series Module, but type 2 in this module
        // TODO: MPPS?
        // TODO: Request AttributesSequence
        // TODO: 10.13 Performed Procedure Step Summary Macro
    }

}
