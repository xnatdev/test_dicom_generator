package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.contextGroups.PetRadionuclide
import org.nrg.dicom.generator.metadata.enums.contextGroups.PetRadiopharmaceutical
import org.nrg.dicom.generator.metadata.enums.contextGroups.RouteOfAdministration
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.protocols.PetStudy
import org.nrg.dicom.generator.metadata.scanners.PetScanner
import org.nrg.dicom.generator.metadata.sequence.RadiopharmaceuticalInformationSequence
import org.nrg.dicom.generator.metadata.series.PtSeries

import java.time.LocalTime

class PetIsotopeModule implements SeriesLevelModule<PtSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, PtSeries series) {
        final PetStudy protocol = study.protocol as PetStudy
        final PetScanner scanner = series.scanner as PetScanner
        final PetRadiopharmaceutical petRadiopharmaceutical = protocol.currentPharmaceutical
        final PetRadionuclide petRadionuclide = petRadiopharmaceutical.petRadionuclide
        final LocalTime injectionTime = series.seriesTime.minusSeconds(protocol.currentInjectionOffset)
        final RadiopharmaceuticalInformationSequence radiopharmaceuticalInformationSequence = new RadiopharmaceuticalInformationSequence()
        final RadiopharmaceuticalInformationSequence.Item sequenceItem = new RadiopharmaceuticalInformationSequence.Item()
        radiopharmaceuticalInformationSequence << sequenceItem
        series.setRadiopharmaceuticalInformationSequence(radiopharmaceuticalInformationSequence)
        sequenceItem.setPetRadionuclide(petRadionuclide)
        if (scanner.encodeRoute()) {
            sequenceItem.setRadiopharmaceuticalRoute(RouteOfAdministration.INTRAVENOUS_ROUTE.codedValue.codeMeaning)
            sequenceItem.setRouteCode(RouteOfAdministration.INTRAVENOUS_ROUTE)
        }
        sequenceItem.setRadiopharmaceuticalStartTime(injectionTime)
        if (scanner.encodeRadiopharmaceuticalStartDateTime()) {
            sequenceItem.setRadiopharmaceuticalStartDateTime(series.seriesDate.atTime(injectionTime))
        }
        sequenceItem.setRadionuclideTotalDose(protocol.currentDosage.toString())
        sequenceItem.setRadionuclideHalfLife(petRadionuclide.halfLife)
        sequenceItem.setRadionuclidePositronFraction(petRadionuclide.positronFraction)
        if (scanner.encodeRadiopharmaceutical()) {
            sequenceItem.setRadiopharmaceutical(petRadiopharmaceutical.codedValue.codeMeaning)
            sequenceItem.setRadiopharmaceuticalCode(petRadiopharmaceutical)
        }
    }

}
