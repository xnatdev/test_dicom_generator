package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule

class MammographySeriesModule implements SeriesLevelModule<MgSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, MgSeries series) {
        // TODO: Request Attributes Sequence
    }

}
