package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.BodyPart
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.series.CrSeries
import org.nrg.dicom.generator.metadata.seriesTypes.cr.CrSeriesType

class CrSeriesModule implements SeriesLevelModule<CrSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, CrSeries series) {
        if (series.bodyPartExamined == null) {
            series.setBodyPartExamined(BodyPart.UNKNOWN) // element overwritten as type 2 by this module
        }
        series.setViewPosition((series.seriesType as CrSeriesType).viewPosition)
        series.setFilterType('0.1Cu,1Al')
        series.setFocalSpots(['2.0'])
        series.setPlateType('Sel Drum 500x500')
    }

}
