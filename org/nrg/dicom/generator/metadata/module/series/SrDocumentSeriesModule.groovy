package org.nrg.dicom.generator.metadata.module.series

import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.series.SrSeries
import org.nrg.dicom.generator.util.RandomUtils

class SrDocumentSeriesModule implements SeriesLevelModule<SrSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, SrSeries series) {
        if (RandomUtils.weightedCoinFlip(20)) {
            series.setReferencedPerformedProcedureStepSopInstanceUid(UIDUtils.createUID())
        }
    }

}
