package org.nrg.dicom.generator.metadata.module.series

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.SeriesLevelModule
import org.nrg.dicom.generator.metadata.series.KoSeries

class KeyObjectDocumentSeriesModule implements SeriesLevelModule<KoSeries> {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, KoSeries series) {
        GeneralSeriesModule.setModality(series)
        GeneralSeriesModule.setSeriesInstanceUid(series)
        GeneralSeriesModule.setSeriesNumber(series)
        GeneralSeriesModule.setSeriesDate(study, series)
        GeneralSeriesModule.setSeriesTime(study, series)
        GeneralSeriesModule.setSeriesDescription(study, series)
        GeneralSeriesModule.setProtocolName(study, series)
        // TODO: MPPS?
    }

}
