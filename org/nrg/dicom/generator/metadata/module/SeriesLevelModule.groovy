package org.nrg.dicom.generator.metadata.module

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.Study

interface SeriesLevelModule<X extends Series> {

    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, X series)

}