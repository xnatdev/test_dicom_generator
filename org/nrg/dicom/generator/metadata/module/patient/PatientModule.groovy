package org.nrg.dicom.generator.metadata.module.patient

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.enums.Sex
import org.nrg.dicom.generator.metadata.module.PatientLevelModule
import org.nrg.dicom.generator.util.RandomUtils

import java.time.LocalDate
import java.util.concurrent.ThreadLocalRandom

class PatientModule implements PatientLevelModule {

    public static final LocalDate leftDobEndpoint = LocalDate.of(1920, 1, 1) // TODO: don't do this
    public static final LocalDate rightDobEndpoint = LocalDate.of(2002, 1, 1) // TODO: don't do this

    // Patient Name, Patient ID, and Ethnic Group are handled outside
    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient) {
        patient.setDateOfBirth(RandomUtils.randomDate(leftDobEndpoint, rightDobEndpoint))
        patient.setSex(ThreadLocalRandom.current().nextBoolean() ? Sex.MALE : Sex.FEMALE)
        patient.setTimeOfBirth(RandomUtils.randomTime())
    }

}
