package org.nrg.dicom.generator.metadata.module

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Series

interface EquipmentLevelModule {

    void apply(SpecificationParameters specificationParameters, Equipment equipment, Series series)

}