package org.nrg.dicom.generator.metadata.module.study

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.NameCache
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Person
import org.nrg.dicom.generator.metadata.Protocol
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.StudyLevelModule
import org.nrg.dicom.generator.util.RandomUtils

import java.time.LocalDate

class GeneralStudyModule implements StudyLevelModule {

    private static final EnumeratedDistribution<Closure<String>> operatorNameFunctionRandomizer = RandomUtils.setupWeightedLottery([
            ({ Person person, boolean supportsNonLatin -> person.serializeToInitials() })             : 60,
            ({ Person person, boolean supportsNonLatin -> person.serializeToDicom(supportsNonLatin) }): 40
    ]) as EnumeratedDistribution<Closure>
    private static final LocalDate studyEndpoint = LocalDate.now().minusMonths(6)

    // Study ID and Accession Number handled outside
    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study) {
        final Protocol protocol = study.protocol
        final Equipment scanner = study.primaryEquipment // if more than one device, they should at least handle all study-level fields the same
        study.setStudyInstanceUid(UIDUtils.createUID())
        study.setStudyDate(RandomUtils.randomDate(patient.earliestAvailableStudyDate, studyEndpoint))
        study.setStudyTime(RandomUtils.randomStudyTime())

        if (protocol.includeMedicalStaff()) {
            if (RandomUtils.weightedCoinFlip(60)) {
                study.setReferringPhysicianName(scanner.serializeName(NameCache.selectPhysician(patient.nationality)))
            }
            if (RandomUtils.weightedCoinFlip(10)) {
                final int numPhysicians = RandomUtils.weightedCoinFlip(90) ? 1 : 2
                study.setConsultingPhysicianName(
                        NameCache.selectPhysicians(patient.nationality, numPhysicians).collect { physician ->
                            physician.serializeToDicom(scanner.supportsNonLatinCharacterSets())
                        }
                )
            }
            final Map<Equipment, List<String>> operatorMap = [:]
            study.equipmentMap.values().unique(false).each { equipment ->
                if (RandomUtils.weightedCoinFlip(95)) {
                    final int numOperators = RandomUtils.weightedCoinFlip(70) ? 1 : 2
                    final Closure<String> operatorSerializer = operatorNameFunctionRandomizer.sample()
                    final List<String> operatorNames = NameCache.selectOperators(equipment.institution, numOperators).collect { operator ->
                        operatorSerializer(operator, equipment.supportsNonLatinCharacterSets()) // belongs to C.7.3.1 General Series Module
                    }
                    operatorMap.put(equipment, operatorNames)
                }
            }
            study.setOperatorMap(operatorMap)
            if (RandomUtils.weightedCoinFlip(95)) {
                final int numPerforming = RandomUtils.weightedCoinFlip(90) ? 1 : 2
                study.setPerformingPhysiciansName(NameCache.selectPhysicians(scanner.institution, numPerforming).collect { physician ->
                    physician.serializeToDicom(scanner.supportsNonLatinCharacterSets()) // belongs to C.7.3.1 General Series Module
                })
            }
        }

        study.setStudyDescription(protocol.getStudyDescription(scanner, study.bodyPartExamined))
    }

}
