package org.nrg.dicom.generator.metadata.module

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Study

interface StudyLevelModule {

    void apply(SpecificationParameters specificationParameters, Patient patient, Study study)

}
