package org.nrg.dicom.generator.metadata.module

import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Patient

interface PatientLevelModule {

    void apply(SpecificationParameters specificationParameters, Patient patient)

}
