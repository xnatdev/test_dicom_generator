package org.nrg.dicom.generator.metadata.module.instance

import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.*
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule

class ImagePixelModule implements InstanceLevelModule {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, Series series, Instance instance) {
        // TODO: this file's original contents have been lost, reconstruct it
    }

}
