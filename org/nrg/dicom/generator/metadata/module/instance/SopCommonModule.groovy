package org.nrg.dicom.generator.metadata.module.instance

import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Instance
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.module.InstanceLevelModule

class SopCommonModule implements InstanceLevelModule {

    @Override
    void apply(SpecificationParameters specificationParameters, Patient patient, Study study, Equipment equipment, Series series, Instance instance) {
        instance.setSopClassUid(series.seriesType.sopClassUid)
        instance.setSopInstanceUid(UIDUtils.createUID())
        // TODO: Instance Creation Date and Time
        instance.setInstanceNumber('1') // TODO
        // TODO: Private Data Element Characteristics Sequence
    }

}
