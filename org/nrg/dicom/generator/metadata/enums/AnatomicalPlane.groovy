package org.nrg.dicom.generator.metadata.enums

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.util.RandomUtils

enum AnatomicalPlane {

    TRANSVERSE (['AX' : 40, 'Ax' : 20, 'Tra' : 20, 'Axial' : 15]),
    CORONAL (['cor' : 30, 'COR' : 30, 'Coronal' : 20]),
    SAGITTAL (['SAG' : 50, 'sag' : 30, 'Sagittal' : 20])

    AnatomicalPlane(Map<String, Integer> valueRepresentationWeights) {
        encodedValueRandomizer = RandomUtils.setupWeightedLottery(valueRepresentationWeights)
    }

    private final EnumeratedDistribution<String> encodedValueRandomizer

    String sampleEncodedValue() {
        encodedValueRandomizer.sample()
    }

}