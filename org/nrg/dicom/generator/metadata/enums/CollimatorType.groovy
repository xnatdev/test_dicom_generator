package org.nrg.dicom.generator.metadata.enums

enum CollimatorType {

    NONE,
    RING

    String getDicomRepresentation() {
        name()
    }

}