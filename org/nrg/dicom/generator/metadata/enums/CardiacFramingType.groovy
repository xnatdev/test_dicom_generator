package org.nrg.dicom.generator.metadata.enums

enum CardiacFramingType {

    FORW,
    BACK,
    PCNT

    String getDicomRepresentation() {
        name()
    }

}