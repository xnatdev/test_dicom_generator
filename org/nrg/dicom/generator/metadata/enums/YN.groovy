package org.nrg.dicom.generator.metadata.enums

enum YN {

    Y, N

    String getDicomRepresentation() {
        name()
    }

}