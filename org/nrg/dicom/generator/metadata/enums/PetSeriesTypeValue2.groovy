package org.nrg.dicom.generator.metadata.enums

enum PetSeriesTypeValue2 {

    IMAGE,
    REPROJECTION

    String getDicomRepresentation() {
        name()
    }

}