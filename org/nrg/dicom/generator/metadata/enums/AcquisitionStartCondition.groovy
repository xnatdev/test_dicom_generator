package org.nrg.dicom.generator.metadata.enums

enum AcquisitionStartCondition {

    DENS,
    RDD,
    MANU,
    TIME,
    AUTO,
    TRIG

    String getDicomRepresentation() {
        name()
    }

}