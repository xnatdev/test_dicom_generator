package org.nrg.dicom.generator.metadata.enums

enum DecayCorrection {

    NONE,
    START,
    ADMIN

    String getDicomRepresentation() {
        name()
    }

}