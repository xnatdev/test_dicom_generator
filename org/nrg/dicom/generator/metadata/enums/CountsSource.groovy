package org.nrg.dicom.generator.metadata.enums

enum CountsSource {

    EMISSION,
    TRANSMISSION

    String getDicomRepresentation() {
        name()
    }

}