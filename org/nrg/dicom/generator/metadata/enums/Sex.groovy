package org.nrg.dicom.generator.metadata.enums

enum Sex {

    MALE ('M'),
    FEMALE ('F')

    final String dicomRepresentation

    Sex(String representation) {
        dicomRepresentation = representation
    }

    @Override
    String toString() {
        dicomRepresentation
    }

}
