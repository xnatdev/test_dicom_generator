package org.nrg.dicom.generator.metadata.enums

enum SecondaryCountsType {

    DLYD,
    SCAT,
    SING,
    DTIM

    String getDicomRepresentation() {
        name()
    }

}