package org.nrg.dicom.generator.metadata.enums

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.util.RandomUtils

/**
 * The goal of this enum is not to model exactly what an "ethnic group" is, but rather to reflect the reality
 * of what ends up in a clinical PACS in that field
 */
enum EthnicGroup {

    AMERICAN_INDIAN (['American Indian' : 100]),
    ASIAN (['ASIAN' : 50, 'Asian' : 30, 'A' : 20]),
    BLACK (['B' : 55, 'BLACK' : 40, 'Black' : 3, 'BLACK OR AFRICAN' : 2]),
    HISPANIC (['HISPANIC' : 60, 'Hispanic' : 40]),
    MULTI_RACIAL (['Multi-Racial' : 100]),
    NON_HISPANIC (['Non-Hispanic' : 80, 'NON-HISPANIC' : 15, 'NOT HISP OR LAT' : 5]),
    OTHER (['OTHER' : 40, 'O' : 30, 'Other' : 30]),
    WHITE (['C' : 33, 'WHITE' : 25, 'W' : 25, 'CAU' : 17])

    EthnicGroup(Map<String, Integer> valueRepresentationWeights) {
        encodedValueRandomizer = RandomUtils.setupWeightedLottery(valueRepresentationWeights)
    }

    private final EnumeratedDistribution<String> encodedValueRandomizer

    String sampleEncodedValue() {
        encodedValueRandomizer.sample()
    }

}
