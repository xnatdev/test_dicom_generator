package org.nrg.dicom.generator.metadata.enums

enum RandomsCorrectionMethod {

    NONE,
    DLYD,
    SING

    String getDicomRepresentation() {
        name()
    }

}