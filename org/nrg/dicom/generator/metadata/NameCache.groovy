package org.nrg.dicom.generator.metadata

import org.nrg.dicom.generator.metadata.enums.Nationality
import org.nrg.dicom.generator.util.RandomUtils

class NameCache {

    private static final int NUM_OPERATORS = 10
    private static final int NUM_HOSPITAL_PHYSICIANS = 15
    private static final int NUM_NATIONAL_PHYSICIANS = 200
    private static final Map<Class<? extends Institution>, List<Person>> operators = [:]
    private static final Map<Class<? extends Institution>, List<Person>> hospitalPhysicians = [:]
    private static final Map<Nationality, List<Person>> generalPhysicians = [:]

    static List<Person> selectOperators(Institution institution, int numOperators) {
        if (!operators.containsKey(institution.class)) {
            operators.put(institution.class, generatePeople(NUM_OPERATORS, institution.commonNationality(), false))
        }
        RandomUtils.randomSubset(operators.get(institution.class), numOperators)
    }

    static Person selectOperator(Institution institution) {
        selectOperators(institution, 1)[0]
    }

    static List<Person> selectPhysicians(Institution institution, int numPhysicians) {
        if (!hospitalPhysicians.containsKey(institution.class)) {
            hospitalPhysicians.put(institution.class, generatePeople(NUM_HOSPITAL_PHYSICIANS, institution.commonNationality(), true))
        }
        RandomUtils.randomSubset(hospitalPhysicians.get(institution.class), numPhysicians)
    }

    static Person selectPhysician(Institution institution) {
        selectPhysicians(institution, 1)[0]
    }

    static List<Person> selectPhysicians(Nationality nationality, int numPhysicians) {
        if (!generalPhysicians.containsKey(nationality)) {
            generalPhysicians.put(nationality, generatePeople(NUM_NATIONAL_PHYSICIANS, nationality, true))
        }
        RandomUtils.randomSubset(generalPhysicians.get(nationality), numPhysicians)
    }

    static Person selectPhysician(Nationality nationality) {
        selectPhysicians(nationality, 1)[0]
    }

    private static List<Person> generatePeople(int numPeople, Nationality nationality, boolean isDoctor) {
        (1 .. numPeople).collect {
            final Person person = nationality.generateRandomPerson()
            if (nationality == Nationality.AMERICAN && isDoctor) {
                person.suffix('M.D.')
            }
            person
        }
    }

}
