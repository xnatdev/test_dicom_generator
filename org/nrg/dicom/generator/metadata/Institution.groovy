package org.nrg.dicom.generator.metadata

import org.nrg.dicom.generator.metadata.enums.Nationality

abstract class Institution {

    abstract String getInstitutionName()

    abstract String getInstitutionAddress()

    Nationality commonNationality() {
        Nationality.AMERICAN
    }

}
