package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.metadata.seriesTypes.xa.XaImageType
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

import static org.nrg.dicom.generator.util.StringReplacements.*

class SiemensAxiomArtis implements SimpleRandomizedTransferSyntaxEquipment, XaScanner {

    private static final EnumeratedDistribution<String> studyDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            'CARDIAC CATHETERIZATION' : 100,
            'Fluoroscopy NoRad' : 50,
            'Fluoroscopy' : 30,
            'FL FLUOROSCOPY' : 30,
            'CARDIAC CATHETERIZATION - DIAGNOSTIC' : 20,
            'Cardiac Cath' : 10,
            'HEART CATH' : 10
    ])

    private static final EnumeratedDistribution<String> seriesDescriptionRandomizer = RandomUtils.setupWeightedLottery([
            (BODYPART) : 100,
            ("${BODYPART} ${FPS}".toString()) : 50,
            'Fluoro Normal' : 50,
            'Fluoro' : 30,
            'FL Normal Dose' : 20,
            ("${BODYPART} ${VIEW}".toString()) : 10,
            'Fluoro   Angio' : 10
    ])

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.JPEGLossless)          : 50,
            (UID.ExplicitVRLittleEndian): 30,
            (UID.ImplicitVRLittleEndian): 10,
            (UID.JPEGExtended24)        : 10
    ])

    private static final List<String> bodyParts = [
            'Aortic Arch',
            "${LATERALITY} Coronary Artery",
            "${LATERALITY} Coronary"
    ]

    private static final List<String> lateralities = ['L', 'Lt', 'LT', 'R', 'Rt', 'RT']
    private static final List<String> views = ['Oblique', 'Obl', 'OBL']
    private static final List<String> fpsValues = ['1', '3', '6', '15']
    private static final List<String> fpsRepresentations = ['F/S', 'fps']

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.SIEMENS_LOWERCASE
    }

    @Override
    String getModelName() {
        'AXIOM-Artis'
    }

    @Override
    String getStationName() {
        'xa-saa0'
    }

    @Override
    String getStudyDescription() {
        studyDescriptionRandomizer.sample()
    }

    @Override
    String getSeriesDescription() {
        seriesDescriptionRandomizer.sample().
                replace(BODYPART, RandomUtils.randomListEntry(bodyParts)).
                replace(LATERALITY, RandomUtils.randomListEntry(lateralities)).
                replace(VIEW, RandomUtils.randomListEntry(views)).
                replace(FPS, "${RandomUtils.randomListEntry(fpsValues)} ${RandomUtils.randomListEntry(fpsRepresentations)}")
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    List<String> getSoftwareVersions() {
        ['VB11A']
    }

    @Override
    String getDeviceSerialNumber() {
        '1323.19909.33087102'
    }

    @Override
    XaImageType getImageType() {
        new XaImageType(original : false, primary : false).
                addValue('SINGLE ' + ThreadLocalRandom.current().nextBoolean() ? 'A' : 'B').
                addValue('STORE MONITOR')
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
