package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.util.RandomUtils

class PhilipsMobileDiagnostwDR implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 50,
            (UID.ImplicitVRLittleEndian) : 30,
            (UID.ExplicitVRBigEndianRetired) : 20
    ])

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.PHILIPS
    }

    @Override
    String getModelName() {
        'MobileDiagnost wDR'
    }

    @Override
    String getStationName() {
        'CAH_XR_P_1A'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        "Portable ${study.bodyPartExamined.code.codeMeaning}"
    }

    @Override
    List<String> getSoftwareVersions() {
        ['1.1.3', 'PMS81.101.1.1 GXR GXRIM10.0'] // copied from a public TCIA example, not really sure what these components are [other than PMS = Philips Medical Systems, probably]
    }

    @Override
    String getDeviceSerialNumber() {
        '7324AE-4100B'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        1
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
