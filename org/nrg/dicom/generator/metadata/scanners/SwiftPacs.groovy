package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.KoNullInstitution
import org.nrg.dicom.generator.util.RandomUtils

/**
 * An entirely made-up PACS that could have generated some auxiliary series
 */
class SwiftPacs implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian.toString()) : 95,
            (UID.ImplicitVRLittleEndian.toString()) : 5
    ])

    @Override
    Institution getInstitution() {
        new KoNullInstitution()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.SWIFT_PACS
    }

    @Override
    String getModelName() {
        'SwiftPACS'
    }

    @Override
    String getStationName() {
        'LTA-A0'
    }

    @Override
    List<String> getSoftwareVersions() {
        ['1.6.5', '2.1a-3.1']
    }

    @Override
    String getDeviceSerialNumber() {
        null
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        1
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
