package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.util.RandomUtils

class LoradSelenia implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.JPEGLossless): 50,
            (UID.ExplicitVRLittleEndian) : 30,
            (UID.ImplicitVRLittleEndian): 10,
            (UID.JPEG2000LosslessOnly) : 10
    ])

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.LORAD
    }

    @Override
    String getModelName() {
        'Lorad Selenia'
    }

    @Override
    String getStationName() {
        'selenia_01'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    List<String> getSoftwareVersions() {
        ['AWS:MAMMODROC_3_4_1_8', 'PXCM:1.4.0.7', 'ARR:1.7.3.10', 'IP:4.9.0'] // copied from a public TCIA example, not really sure what these components are
    }

    @Override
    String getDeviceSerialNumber() {
        'H1KRHR8426d1b2'
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
