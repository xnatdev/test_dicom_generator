package org.nrg.dicom.generator.metadata.scanners

import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.institutions.StRomanWest

class StRomanWestAchieva extends PhilipsAchieva {

    @Override
    Institution getInstitution() {
        new StRomanWest()
    }

    @Override
    String getStationName() {
        'PHILIPS-AA00005'
    }

    @Override
    String getDeviceSerialNumber() {
        '22528'
    }

}
