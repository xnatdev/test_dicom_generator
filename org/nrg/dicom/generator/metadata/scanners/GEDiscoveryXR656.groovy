package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

class GEDiscoveryXR656 implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 50,
            (UID.ImplicitVRLittleEndian) : 30,
            (UID.ExplicitVRBigEndianRetired) : 20
    ])

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.GE_HEALTHCARE
    }

    @Override
    String getModelName() {
        'Discovery XR656'
    }

    @Override
    String getStationName() {
        'CAH_XR_S_1'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        "${study.bodyPartExamined.dicomRepresentation}_1_VIEW"
    }

    @Override
    List<String> getSoftwareVersions() {
        ['dm_Platform_release-FW01_2-1985']
    }

    @Override
    String getDeviceSerialNumber() {
        '938814ZC9'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        ThreadLocalRandom.current().nextInt(10000)
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
