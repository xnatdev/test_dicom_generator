package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.RtNullInstitution
import org.nrg.dicom.generator.util.RandomUtils

class AdacPinnacle3 implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian.toString()) : 80,
            (UID.ImplicitVRLittleEndian.toString()) : 20
    ])

    @Override
    Institution getInstitution() {
        new RtNullInstitution()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.ADAC
    }

    @Override
    String getModelName() {
        'Pinnacle3'
    }

    @Override
    String getStationName() {
        'RT-W-43b'
    }

    @Override
    List<String> getSoftwareVersions() {
        ['9.6', '9.6']
    }

    @Override
    String getDeviceSerialNumber() {
        'W8M5FVMKYJHT'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        null
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        1
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
