package org.nrg.dicom.generator.metadata.scanners

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.util.RandomUtils

class AcusonSequoia implements Equipment {

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.ACUSON
    }

    @Override
    String getModelName() {
        'SEQUOIA'
    }

    @Override
    String getStationName() {
        'US_MOD-2'
    }

    @Override
    String getProtocolName(Study study, Series seriesType) {
        null
    }

    @Override
    List<String> getSoftwareVersions() {
        ['3.15']
    }

    @Override
    String getDeviceSerialNumber() {
        '51445'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        1
    }

    @Override
    @SuppressWarnings('GroovyMissingReturnStatement')
    String getTransferSyntaxUID(String sopClassUID) {
        switch (sopClassUID) {
            case UID.UltrasoundImageStorage :
                return RandomUtils.weightedCoinFlip(80) ? UID.RLELossless : UID.ImplicitVRLittleEndian
            case UID.UltrasoundMultiFrameImageStorage :
                return RandomUtils.weightedCoinFlip(90) ? UID.JPEGBaseline1 : UID.ImplicitVRLittleEndian
            default :
                reportUnsupportedSopClass(sopClassUID)
        }
    }

}
