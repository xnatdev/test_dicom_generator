package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.ImageType
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.enums.RandomsCorrectionMethod
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.metadata.sequence.EnergyWindowRangeSequence
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtSeriesType
import org.nrg.dicom.generator.util.RandomUtils

class SiemensBiographTruePoint64 extends SiemensPetDevice implements CtScanner {

    private String currentImagingTransferSyntax
    private String currentNonimagingTransferSyntax

    private static final EnumeratedDistribution<String> imagingTransferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 100,
            (UID.JPEGLossless) : 50,
            (UID.JPEGBaseline1) : 50,
            (UID.ImplicitVRLittleEndian) : 10,
            (UID.JPEGExtended24) : 10
    ])
    private static final EnumeratedDistribution<String> nonimagingTransferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 90,
            (UID.ImplicitVRLittleEndian) : 10
    ])

    private static final List<Integer> seriesNumbers = [1, 1, 2, 4, 5, 501]

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.SIEMENS
    }

    @Override
    String getModelName() {
        'Biograph64_TruePoint'
    }

    @Override
    String getStationName() {
        'PETCT_001'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        readSeriesNumberFromIndexedList(seriesNumbers, seriesIndex)
    }

    @Override
    ImageType getTopogramImageType() {
        getCtImageType(true).addValue('CT_SOM5 TOP')
    }

    @Override
    ImageType getAttenuationCorrectedCtImageType() {
        getCtImageType(false).addValue('CT_SOM5 SPI')
    }

    @Override
    String getProtocolName(Study study, Series series) {
        study.studyDescription.split('\\^').last()
    }

    @Override
    List<String> getSoftwareVersions() {
        ['VG10A']
    }

    @Override
    String getDeviceSerialNumber() {
        '34566'
    }

    @Override
    void resample() {
        currentImagingTransferSyntax = imagingTransferSyntaxRandomizer.sample()
        currentNonimagingTransferSyntax = nonimagingTransferSyntaxRandomizer.sample()
    }

    @Override
    String getTransferSyntaxUID(String sopClassUID) {
        switch (sopClassUID) {
            case UID.PrivateSiemensCSANonImageStorage : // only supported non-imaging instance here for now
                return currentNonimagingTransferSyntax
            default :
                return currentImagingTransferSyntax
        }
    }

    @Override
    RandomsCorrectionMethod getRandomsCorrectionMethod() {
        RandomsCorrectionMethod.DLYD
    }

    @Override
    String getAttenuationCorrectionMethod(PtSeriesType seriesType) {
        (seriesType.isAttenuationCorrected()) ? 'CT-derived mu-map' : null
    }

    @Override
    List<String> getConvolutionKernel() {
        ['XYZ G5.00']
    }

    @Override
    String getReconstructionMethod() {
        'PSF 4i21s'
    }

    @Override
    String getAxialAcceptance() {
        '38'
    }

    @Override
    EnergyWindowRangeSequence getEnergyWindowRangeSequence() {
        final EnergyWindowRangeSequence sequence = new EnergyWindowRangeSequence()
        sequence << new EnergyWindowRangeSequence.Item(lowerLimit : '435', upperLimit: '650')
        sequence
    }

    @Override
    boolean encodeRadiopharmaceutical() {
        false
    }

}
