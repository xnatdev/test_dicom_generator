package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.util.RandomUtils

class GEOptimaMR450w implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian): 90,
            (UID.ImplicitVRLittleEndian): 10
    ])
    private static final List<Integer> seriesNumbers = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 601, 602, 1101]

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.GE
    }

    @Override
    String getModelName() {
        'Optima MR450w'
    }

    @Override
    String getStationName() {
        'GEMS1234'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    List<String> getSoftwareVersions() {
        ['14', 'LX', 'MR Software release:14.0_M4_0620.a']
    }

    @Override
    String getDeviceSerialNumber() {
        '0000000291844MR2'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        readSeriesNumberFromIndexedList(seriesNumbers, seriesIndex)
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
