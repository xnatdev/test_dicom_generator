package org.nrg.dicom.generator.metadata.scanners

import com.fasterxml.jackson.annotation.JsonIgnore
import org.nrg.dicom.generator.metadata.seriesTypes.xa.XaImageType

interface XaScanner {

    @JsonIgnore
    String getStudyDescription()

    @JsonIgnore
    String getSeriesDescription()

    @JsonIgnore
    XaImageType getImageType()

}
