package org.nrg.dicom.generator.metadata.scanners

import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.institutions.VandeventerRegionalHospital

class VandeventerRegionalHospitalAvanto extends SiemensAvanto {

    @Override
    Institution getInstitution() {
        new VandeventerRegionalHospital()
    }

    @Override
    String getStationName() {
        'MRC70990'
    }

    @Override
    String getDeviceSerialNumber() {
        'S-000431249820451709-B098'
    }

}
