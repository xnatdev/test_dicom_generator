package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.enums.RandomsCorrectionMethod
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.metadata.sequence.EnergyWindowRangeSequence
import org.nrg.dicom.generator.metadata.seriesTypes.pt.PtSeriesType
import org.nrg.dicom.generator.metadata.seriesTypes.sr.PhoenixZIPReport
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

class SiemensBiographmMR extends SiemensPetDevice {

    private String currentImagingTransferSyntax
    private String currentNonimagingTransferSyntax

    private static final EnumeratedDistribution<String> imagingTransferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 100,
            (UID.JPEGLossless) : 50,
            (UID.JPEGBaseline1) : 50,
            (UID.ImplicitVRLittleEndian) : 10,
            (UID.JPEGExtended24) : 10
    ])
    private static final EnumeratedDistribution<String> nonimagingTransferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 90,
            (UID.ImplicitVRLittleEndian) : 10
    ])
    private static final EnumeratedDistribution<String> convolutionKernelRandomizer = RandomUtils.setupWeightedLottery([
            'XYZGAUSS3.00' : 60,
            'XYZGAUSS4.00' : 30,
            'XYZBOX' : 10
    ])

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.SIEMENS
    }

    @Override
    String getModelName() {
        'Biograph_mMR'
    }

    @Override
    String getStationName() {
        'PETMR-STAT-1'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        (seriesType instanceof PhoenixZIPReport) ? 99 : seriesIndex + 1
    }

    @Override
    String getProtocolName(Study study, Series series) {
        ThreadLocalRandom.current().nextBoolean() ? series.seriesDescription : series.seriesDescription + '_1'
    }

    @Override
    List<String> getSoftwareVersions() {
        ['syngo MR B20P']
    }

    @Override
    String getDeviceSerialNumber() {
        'X1-43-9901'
    }

    @Override
    void resample() {
        currentImagingTransferSyntax = imagingTransferSyntaxRandomizer.sample()
        currentNonimagingTransferSyntax = nonimagingTransferSyntaxRandomizer.sample()
    }

    @Override
    String getTransferSyntaxUID(String sopClassUID) {
        switch (sopClassUID) {
            case UID.EnhancedSRStorage : // only supported non-imaging instance here for now
                return currentNonimagingTransferSyntax
            default :
                return currentImagingTransferSyntax
        }
    }

    @Override
    RandomsCorrectionMethod getRandomsCorrectionMethod() {
        RandomsCorrectionMethod.DLYD
    }

    @Override
    String getAttenuationCorrectionMethod(PtSeriesType seriesType) {
        null
    }

    @Override
    List<String> getConvolutionKernel() {
        [convolutionKernelRandomizer.sample()]
    }

    @Override
    String getReconstructionMethod() {
        'OSEM3D 2i8s'
    }

    @Override
    String getAxialAcceptance() {
        '60'
    }

    @Override
    EnergyWindowRangeSequence getEnergyWindowRangeSequence() {
        final EnergyWindowRangeSequence sequence = new EnergyWindowRangeSequence()
        sequence << new EnergyWindowRangeSequence.Item(lowerLimit : '430', upperLimit: '610')
        sequence
    }

    @Override
    boolean encodeRadiopharmaceutical() {
        true
    }

}
