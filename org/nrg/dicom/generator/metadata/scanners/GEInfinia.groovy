package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital
import org.nrg.dicom.generator.metadata.seriesTypes.nm.MyometrixResults
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

class GEInfinia implements SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.ExplicitVRLittleEndian) : 90,
            (UID.ImplicitVRLittleEndian) : 10
    ])

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.GE
    }

    @Override
    String getModelName() {
        'INFINIA'
    }

    @Override
    String getStationName() {
        '93_GENM_INFI'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        'NM_MYOCARDIAL_PERFUSION'
    }

    @Override
    List<String> getSoftwareVersions() {
        ['2.105.030.18']
    }

    @Override
    String getDeviceSerialNumber() {
        '16312'
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        (seriesType instanceof MyometrixResults) ? 100000000 + ThreadLocalRandom.current().nextInt(900000000) : 1
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

}
