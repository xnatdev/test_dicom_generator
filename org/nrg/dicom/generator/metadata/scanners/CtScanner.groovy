package org.nrg.dicom.generator.metadata.scanners

import com.fasterxml.jackson.annotation.JsonIgnore
import org.nrg.dicom.generator.metadata.ImageType

trait CtScanner {

    @JsonIgnore
    abstract ImageType getTopogramImageType()

    @JsonIgnore
    abstract ImageType getAttenuationCorrectedCtImageType()

    ImageType getCtImageType(boolean isLocalizer) {
        new ImageType().addValue(isLocalizer ? 'LOCALIZER' : 'AXIAL')
    }

}