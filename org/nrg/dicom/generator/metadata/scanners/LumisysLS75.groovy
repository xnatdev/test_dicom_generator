package org.nrg.dicom.generator.metadata.scanners

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.dcm4che3.data.Attributes
import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.SecondaryCaptureEquipment
import org.nrg.dicom.generator.metadata.SimpleRandomizedTransferSyntaxEquipment
import org.nrg.dicom.generator.metadata.enums.ConversionType
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.util.RandomUtils

@SuppressWarnings('ClashingTraitMethods') // default resolution is correct here
class LumisysLS75 implements SecondaryCaptureEquipment, SimpleRandomizedTransferSyntaxEquipment {

    private static final EnumeratedDistribution<String> transferSyntaxRandomizer = RandomUtils.setupWeightedLottery([
            (UID.JPEGLossless) : 50,
            (UID.JPEGExtended24): 30,
            (UID.JPEGBaseline1): 10,
            (UID.ImplicitVRLittleEndian)  : 10
    ])

    @Override
    String getDeviceId() {
        'LLS-0001'
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.LUMISYS
    }

    @Override
    String getModelName() {
        'LS75'
    }

    @Override
    List<String> getSoftwareVersions() {
        ['LUMISYS RT-2000 v1.0']
    }

    @Override
    ConversionType getConversionType() {
        ConversionType.DIGITIZED_FILM
    }

    @Override
    EnumeratedDistribution<String> getTransferSyntaxRandomizer() {
        transferSyntaxRandomizer
    }

    @Override
    void encode(Attributes attributes) {
        encodeSecondaryCaptureEquipment(attributes)
    }

}
