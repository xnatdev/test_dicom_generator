package org.nrg.dicom.generator.metadata.scanners

import org.dcm4che3.data.UID
import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Equipment
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.SeriesType
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.enums.Manufacturer
import org.nrg.dicom.generator.metadata.institutions.ChestertonAdamsHospital

class PhilipsAchieva implements Equipment {

    @Override
    Institution getInstitution() {
        new ChestertonAdamsHospital()
    }

    @Override
    Manufacturer getManufacturer() {
        Manufacturer.PHILIPS
    }

    @Override
    String getModelName() {
        'Achieva'
    }

    @Override
    String getStationName() {
        'PHILIPS-8A11CE4'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    List<String> getSoftwareVersions() {
        ['2.6.3', '2.6.3.3']
    }

    @Override
    int getSeriesNumber(int seriesIndex, SeriesType seriesType) {
        "${seriesIndex + 4}01".toInteger()
    }

    @Override
    String getDeviceSerialNumber() {
        '26344'
    }

    @Override
    String getTransferSyntaxUID(String sopClassUID) {
        UID.ExplicitVRLittleEndian
    }

}
