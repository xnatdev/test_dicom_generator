package org.nrg.dicom.generator.metadata.scanners

import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.institutions.CenterForSpecializedRadiology

class OutsideGEInfinia extends GEInfinia {

    @Override
    Institution getInstitution() {
        new CenterForSpecializedRadiology()
    }

    @Override
    String getStationName() {
        'MPI-NM-01'
    }

    @Override
    String getDeviceSerialNumber() {
        '12911'
    }

}
