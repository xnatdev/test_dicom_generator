package org.nrg.dicom.generator.metadata.scanners

import org.nrg.dicom.generator.metadata.Institution
import org.nrg.dicom.generator.metadata.Series
import org.nrg.dicom.generator.metadata.Study
import org.nrg.dicom.generator.metadata.institutions.WestAthensGeneralHospital

class GreekSiemensAvanto extends SiemensAvanto {

    @Override
    Institution getInstitution() {
        new WestAthensGeneralHospital()
    }

    @Override
    String getStationName() {
        'MRC27136'
    }

    @Override
    String getProtocolName(Study study, Series series) {
        series.seriesDescription
    }

    @Override
    List<String> getSoftwareVersions() {
        ['syngo MR D12']
    }

    @Override
    String getDeviceSerialNumber() {
        'S-000423719840917111-D188'
    }

    @Override
    boolean supportsNonLatinCharacterSets() {
        true
    }

}
