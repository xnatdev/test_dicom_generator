## Overview:

The primary goal of this project is to generate large DICOM datasets to simulate a wide variety of data you may find on a clinical PACS. There are significant limitations to keep in mind, as the data is primarily modeled at the study, series, and equipment levels.

## Study Types implemented:

*First note that elements at the instance level are primarily not implemented. For example, magnetic field strength and scanning sequence are very important for MR data, but all of these types of elements have to be modeled, interrelated, and randomized based on SOP class. These elements are in general not even included in the below lists, since they would take a very, very large amount of work to implement.*

Implemented:

* **MG**: Mammogram with randomly chosen single view position.
* **MG**: Mammogram with 4 standardized view positions.
* **MR**: A complex MRI protocol consisting of:
	+ Transverse Localizer
	+ Transverse T1w
	+ Transverse T2w
	+ Transverse DWI
	+ Sagittal DTI
	+ Transverse FLAIR
	+ Sagittal T2\*
	+ Transverse T1w with contrast
	+ Coronal BOLD
* **MR**: A simple fast MRI protocol consisting of:
	+ Transverse Localizer
	+ Transverse T2w
	+ Transverse T1w
* **MR:** A copy of the previous protocol except it's acquired at one of a few different outside hospitals.
* **MR**: A diffusion and perfusion heavy brain-only MRI protocol:
	+ Sagittal Localizer
	+ Sagittal T1w
	+ Transverse SWI
	+ Sagittal DCE
	+ Sagittal ADC
	+ Sagittal DSC
* **PT,CT**: A PET/CT simulated from a combined dual-modality Siemens scanner with series:
	+ Topogram
	+ Siemens (Proprietary SOP Class) VOI capture
	+ CT w/ Attenuation Correction
	+ PT w/ Attenuation Correction
	+ PT w/o Attenuation Correction
	+ "Patient Protocol" secondary capture
* **CR**: An xray simulated from a mobile Philips scanner with a single anteroposterior view of the chest or abdomen
* **DX**: An xray simulated from a GE scanner with a single view of one of the following body parts (selected randomly):
	+ Abdomen
	+ Cervical Spine
	+ Chest
	+ Skull
	+ Spine
	+ Thoracic Spine
* **NM**: A myocardial perfusion study with resting and gated series simulated from a GE scanner.
* **NM**: A copy of the previous protocol except it's acquired at an outside hospital.
* **US:** A single series standard ultrasound simulated from either an Acuson or Philips scanner.
* **XA:** An xray angiography/fluoroscopy study simulated from either a Philips or Siemens scanner with:
	+ 10 series for studies simulated from the main hospital
	+ 15 series for studies simulated from an outside institution
* **PT,MR,SR:** A PET-MR study simulated from a Siemens PET-MR scanner. Also includes a "Phoenix Evidence Document" structured report (without any of the structured report data, see the note in italics).
* **<Various>:** Digitized film secondary capture objects for two different types of acquisition:
	+ **MG/OT/SC:** A digitized film mammogram with a modality value selected randomly from the list.
	+ **CR/DX/OT/SC/XR:** A digitized film xray with a modality value selected randomly from the list.
* **<Various>**: Several of the previous study types are also simulated from hospitals outside the US, including Japan, Korea, and Greece. Names of both patients and physicians are localized and written in non-Latin character sets.
* **MR,RTSTRUCT,KO:** An MR study with contours added later via RTSTRUCT object (note that the contours are in the equivalent of an instance level attribute so the contours themselves are not actually included), and then a Key Object note added later by an intermediate PACS.
* **MG,PR:** A mammogram with annotations added later.

## Perturbations

In the real world (i.e. on a clinical PACS), you'll find the same thing encoded multiple ways at different times, or sometimes missing entirely. To be as realistic as possible, there are some "perturbations" intentionally introduced in the data:

1. Patient name. A patient name is written "Last^First" most of the time, but a small percent of the time, they may have their middle initial or full middle name added, and/or the whole string will be coerced to uppercase. (There's also international scanners which include the ideographic/phonetic name representations).
2. Ethnic group: a patient is assigned one ethnic group, but it may be entered differently for each study to match what the technician is entered (e.g. on study 1, the patient is "White", but on study 2, the patient is "W").
3. Patient age is sometimes left out of the DICOM for a study. The percent of this happening is configurable.
4. Referring Physician, Performing Physician(s), and Operators are sometimes included, and sometimes suppressed at the study level.
5. Patient Weight and Patient Size (height) are configurable for the probability to include them. Additionally, they're further configurable on probability to encode them as "0.0" to match the real world.

---

## Possible updates to make

1. Add frame of reference modules.

## Possible features/improvements that require a lot of work:

1. Allow imaging studies for children and infants. Requires:
	1. Adding complexity to the patient age serialization.
	2. Makes adding weight/height to the DICOM more difficult
	3. Requires more complexity in deciding which protocols/modalities are appropriate for a patient (a 2 month old is not going to get a mammogram).
2. Remove hard-coding that each series lasts for 5 minutes. This requires research on how long all of the series modeled so far will last.
3. Adding instance-level specific fields.
4. Pixel data that represents the instances/series well.

## Physique Determination

From the document here <https://www.cdc.gov/nchs/data/series/sr_03/sr03_039.pdf,> we take the mean height for males at ages 16, 17, 18, 19 from table 7. From table 11 we take each of the ranges (20-29, 30-39, etc.) and assign that value to the midpoint to get mean heights at ages, 25, 35, ..., 75. Finally, from the "80 years and older" value, we make up some data points for ages 85, 95, 105. With all of these data points, we can create a spline interpolator for any age between 16-105. We repeat the same for females from the other tables. The spline interpolators give us a way to get a reasonable "reference height" for a person given sex + age.

1. First, for any given patient, they are given some "modifiers" representing individual physique deviations from the mean:
	1. The patient is assigned a "height modifier" *h* sampled from the normal distribution with mean 0 and variance 1 constrained to the range [-3, 3].
	2. The patient is assigned a "weight modifier" *w*. First, a value *b* is sampled from the beta distribution with *α = 2, β = 5*. Then, we assign *w = 10b - 2*. The beta distribution with the chosen parameters was selected to give the desired shape of the distribution. It allows for rarer but very large modifier values in the positive direction only. This is to model real life because you can have a patient who is very, very, heavy, but you can't have one who is 1.8 meters and 5 kilograms. The affine transformation after sampling from the distribution allows us to shift the range of values from [0, 1] to [-2, 8].
2. For a given study, we use the patient sex and age to determine the "reference height" *r* from the spline interpolation mentioned above.
3. Then, we assign the patient a size of *r + 0.05(h + d)*, where *d* is randomly selected between -0.5 and 0.5. This allows for reasonable variations in height while still maintaining the individual's height trajectory.
4. For weights, obtaining a reference weight is easier. From the chart at <https://www.nhs.uk/live-well/healthy-weight/height-weight-chart,> using a linear (affine) approximation between weight and height is reasonable. We take two points in the "overweight" category to determine our line: height = 1.5m, weight = 62.0kg and height = 2.0m, weight = 110.0kg. From that line, we have the patient's assigned height, so we can easily get a reference weight *g*.
5. Finally, similarly to before, we assign the final weight of *g + 10(w + n)* where again *n* is randomly selected between -0.5 and 0.5.

## Pixel Data

In the request specification YAML, a boolean (**includePixelData**) can be set to true. Currently, this will download a frame from NRG's "sample1" reference dataset and include the Pixel Data (and related metadata such as Rows, Columns, etc.) from the downloaded frame in every generated instance which should use the Image Pixel Module (e.g. MR, PT, CT, ... but not RTSTRUCT, KO, ...). This setting is disabled by default as the data will not match the Pixel Data well at all, so the generated output will be suboptimal, but it can be enabled in case junk pixel data is acceptable. Another caveat emptor: the transfer syntax encoded is a random variable, so there may also be conflicts where an instance claims to have a JPEG transfer syntax but the pixel data is just raw little endian explicit VR.

## DicomBrowser view of some sample output

For ~250 patients:

 ![](./assets/250_patients.png)

  


For 1 patient:

 ![](./assets/1_patient.png) 

  


For one study:

 ![](./assets/1_study.png) 

