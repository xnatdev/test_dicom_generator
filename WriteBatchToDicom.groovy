import org.nrg.dicom.generator.BatchSpecification
import org.nrg.dicom.generator.YamlObjectMapper

@GrabResolver(name='dcm4che repo', root='https://www.dcm4che.org/maven2', m2Compatible=true)
@Grapes([
        @Grab(group='com.fasterxml.jackson.core', module='jackson-databind', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.dataformat', module='jackson-dataformat-yaml', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.datatype', module='jackson-datatype-jsr310', version='2.10.2'),
        @Grab(group='org.dcm4che', module='dcm4che-core', version='5.21.0'),
        @Grab(group='org.apache.commons', module='commons-math3', version='3.6.1')
])
final CliBuilder cliBuilder = new CliBuilder(usage: 'WriteBatchToDicom.groovy <batch files separated by spaces>')
final OptionAccessor options = cliBuilder.parse(args)

final YamlObjectMapper objectMapper = new YamlObjectMapper()
final File outputDir = new File('dicom_output')
if (!outputDir.exists()) {
    outputDir.mkdir()
}

final List<String> batchFiles = options.arguments()

println("STAGE 2: ${batchFiles.size()} batch file${batchFiles.size() > 1 ? 's' : ''} will be written to DICOM.")

batchFiles.eachWithIndex { batchFile, index ->
    objectMapper.readValue(new File(batchFile), BatchSpecification).generateDicom(index, batchFiles.size(), outputDir)
}
