import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.nrg.dicom.generator.BatchSpecification
import org.nrg.dicom.generator.IdReprocessor
import org.nrg.dicom.generator.SpecificationParameters
import org.nrg.dicom.generator.YamlObjectMapper
import org.nrg.dicom.generator.metadata.Patient
import org.nrg.dicom.generator.metadata.SortableStudy
import org.nrg.dicom.generator.metadata.patient.*
import org.nrg.dicom.generator.util.RandomUtils

import java.util.concurrent.ThreadLocalRandom

@GrabResolver(name='dcm4che repo', root='https://www.dcm4che.org/maven2', m2Compatible=true)
@Grapes([
        @Grab(group='com.fasterxml.jackson.core', module='jackson-databind', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.dataformat', module='jackson-dataformat-yaml', version='2.10.2'),
        @Grab(group='com.fasterxml.jackson.datatype', module='jackson-datatype-jsr310', version='2.10.2'),
        @Grab(group='org.dcm4che', module='dcm4che-core', version='5.21.0'),
        @Grab(group='org.apache.commons', module='commons-math3', version='3.6.1')
])
final CliBuilder cliBuilder = new CliBuilder(usage: 'GenerateDataSpecification.groovy <parameters>')

cliBuilder.with {
    w('Also write DICOM to files instead of just creating manifests', required: false)
    f('Fix values in Study ID and Patient ID to be more realistic (requires an extra round of re-parsing intermediate YAML files)', required: false)
    c('Config file specifying data request', args: 1, required: true)
}

final OptionAccessor options = cliBuilder.parse(args)

long generatedPatients = 0
long generatedStudies = 0
long generatedSeries = 0
long nextPatientId = 2000000000 + ThreadLocalRandom.current().nextLong(7000000000)
long nextAccessionNumber = 2000000000 + ThreadLocalRandom.current().nextLong(7000000000)

final boolean fixIds = options.f ?: false
final List<SortableStudy> studyRecords = fixIds ? [] : null
final SpecificationParameters fullSpecification = new YamlObjectMapper().readValue(new File(options.c), SpecificationParameters)
fullSpecification.postprocess()
BatchSpecification currentBatch = new BatchSpecification(id : 0)

new File('batches').mkdir()
final int expectedNumBatches = Math.ceil(fullSpecification.numPatients / BatchSpecification.MAX_PATIENTS).intValue()

println("STAGE 1: you requested data for ${fullSpecification.numPatients} patients. Manifests for creating the DICOM will first be generated in about ${expectedNumBatches} batch${expectedNumBatches > 1 ? 'es' : ''}")

final EnumeratedDistribution<PatientRandomizer> patientRandomizers = RandomUtils.setupWeightedLottery([
        (new DefaultPatientRandomizer()) : 85,
        (new GreekPatientRandomizer()) : 5,
        (new JapanesePatientRandomizer()): 5,
        (new KoreanPatientRandomizer()): 5
]) as EnumeratedDistribution<PatientRandomizer>

while (generatedPatients < fullSpecification.numPatients || generatedStudies < fullSpecification.numStudies || generatedSeries < fullSpecification.numSeries) {
    final int currentAverageStudiesPerPatient = generatedPatients == 0 ? 0 : generatedStudies / generatedPatients
    final Patient patient = patientRandomizers.sample().createPatient(fullSpecification)
    if (!fixIds) {
        patient.setPatientId(nextPatientId.toString())
        nextPatientId++
    }
    patient.randomize(fullSpecification, currentAverageStudiesPerPatient, generatedSeries, generatedStudies, studyRecords)
    currentBatch.patients << patient
    generatedPatients++
    patient.studies.each { study ->
        if (!fixIds) {
            study.setAccessionNumber(nextAccessionNumber.toString())
            study.setStudyId(study.accessionNumber)
            nextAccessionNumber++
        }
        generatedStudies++
        generatedSeries += study.series.size()
    }
    if (currentBatch.isFull()) {
        currentBatch.writeToFileAndLog(expectedNumBatches)
        currentBatch = new BatchSpecification(id : currentBatch.id + 1)
    }
}

if (!currentBatch.written && currentBatch.patients.size() > 0) {
    currentBatch.writeToFileAndLog(expectedNumBatches)
}

if (options.f) {
    println("Study/Patient ID values will now be corrected based on Study Date/Time. Reprocessing all batch YAML files...")
    new IdReprocessor().reprocessIds(nextPatientId, nextAccessionNumber, studyRecords)
}

println("STAGE 1 COMPLETE: manifests for data of ${fullSpecification.numPatients} patients have been created.")

if (options.w ?: false) {
    run(
            new File('WriteBatchToDicom.groovy'),
            (0 .. currentBatch.id).collect { id ->
                new BatchSpecification(id : id).relativeFilePath()
            } as String[]
    )
}
